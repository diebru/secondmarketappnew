package com.example.seconhandmarketapp

import android.content.pm.PackageManager
import android.location.Location
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng


class MyViewModel : ViewModel() {
    var fragmentOnSale = false
    var fragmentItemList = false
    var fragmentInterestList = false
    var fragmentBought = false
    var fragmentProfile = false
    var other = false
    var close = false

    lateinit var fusedLocationClient: FusedLocationProviderClient
    var currentLoc : LatLng = LatLng(0.0, 0.0)

    fun setOnSale() {
        fragmentOnSale = true
        fragmentItemList = false
        fragmentInterestList = false
        fragmentBought = false
        fragmentProfile = false
        other = false
        close = false
    }

    fun setItemList() {
        fragmentOnSale = false
        fragmentItemList = true
        fragmentInterestList = false
        fragmentBought = false
        fragmentProfile = false
        other = false
        close = false
    }

    fun setInterestList() {
        fragmentOnSale = false
        fragmentItemList = false
        fragmentInterestList = true
        fragmentBought = false
        fragmentProfile = false
        other = false
        close = false
    }

    fun setBought() {
        fragmentOnSale = false
        fragmentItemList = false
        fragmentInterestList = false
        fragmentBought = true
        fragmentProfile = false
        other = false
        close = false
    }

    fun setProfile() {
        fragmentOnSale = false
        fragmentItemList = false
        fragmentInterestList = false
        fragmentBought = false
        fragmentProfile = true
        other = false
        close = false
    }

    fun setother(b : Boolean) {
        other = b
    }

    fun setClose() {
        close = true
    }

    fun updateCurrentLoc() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null)
                    currentLoc = LatLng(location.latitude, location.longitude)
            }
            .addOnFailureListener{
            }
    }
}