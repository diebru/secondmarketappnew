package com.example.seconhandmarketapp


import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_item_list.*
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.RuntimeException


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ItemListFragment.OnListFragmentInteractionListener] interface.
 */


class BoughtItemsListFragment : Fragment() {
    private val db = Firebase.firestore
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyItemRecyclerViewAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var l = listOf<Item>()
    private var TAG = "users"
    private var first = false
    private val storage = Firebase.storage
    var storageRef = storage.reference
    private val vm: MyViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setBought()
    }

    override fun onResume() {
        super.onResume()
        vm.setBought()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR

        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        viewManager = LinearLayoutManager(context)
        viewAdapter = MyItemRecyclerViewAdapter(l, null, null, null, listener, false, false, true)

        recyclerView = view.findViewById<RecyclerView>(R.id.list).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        var fab: FloatingActionButton = view.fab_list
        fab.visibility = View.GONE
        fab = view.fab_refresh
        fab.visibility = View.GONE



        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(BoughtList.update) {
            BoughtList.clear()
            realtimeUpdateDB()
        } else {
            if (BoughtList.isEmpty())
                realtimeUpdateDB()
            else {
                viewAdapter.setItems(BoughtList.getItemList())
                view.noItemLabel.text = ""
            }
        }

        view.swipe_layout.setOnRefreshListener {
            realtimeUpdateDB()
            view.swipe_layout.isRefreshing=false
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BoughtItemsListFragment.OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun realtimeUpdateDB() {
        db.collectionGroup("items").addSnapshotListener { documents, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed", e)
                    return@addSnapshotListener
                }
                BoughtList.clear()
                BoughtList.setUpd(false)
                for (item in documents!!) {
                    if (item.data["userID"] != Users.getUser() && item.data["isSold"] == true && item.data["buyer"] == Users.getUser()) {
                        BoughtList.addItem(Item(item.data["itemID"].toString(),
                            item.data["userID"].toString(),
                            "@drawable/itemicon",
                            item.data["title"].toString(),
                            item.data["description"].toString(),
                            item.data["price"].toString(),
                            item.data["location"].toString(),
                            item.data["expirydate"].toString(),
                            item.data["category"].toString(),
                            item.data["isSold"] as Boolean,
                            item.data["isHide"] as Boolean,
                            item.data["interestedUsers"] as ArrayList<String>)
                        )
                    }
                    Log.d(TAG, "${item.id} => ${item.data}")
                }
                viewAdapter.setItems(BoughtList.getItemList())
                downloadPhoto()
                if(noItemLabel!= null) {
                    if (!BoughtList.isEmpty())
                        noItemLabel.text = ""
                    else
                        noItemLabel.text = "No item to show"
                }
                Log.d(TAG, "Listen succeed")
            }
    }

    private fun downloadPhoto()
    {
        val localFileList: MutableList<File> = arrayListOf()
        val path: MutableList<String> = arrayListOf()
        for ((i, item) in BoughtList.list.withIndex()) {
            MainScope().launch {
                path.add("@drawable/itemicon")
                localFileList.add(File.createTempFile("image" + i, "jpg"))
                withContext(Dispatchers.IO) {
                    storageRef.child("user_images/" + item.userID + "/" + item.id + ".jpg")
                        .getFile(localFileList[i])
                        .addOnSuccessListener {
                            path[i] = localFileList[i].absolutePath
                            Log.d("foto", path[i])
                            BoughtList.modifyItem(Item(item.id.toString(), item.userID, path[i], item.titolo!!, item.desc!!, item.prezzo!!, item.loc!!, item.exp!!,
                                item.categoria!!, item.isSold, item.isHide, item.interestedUsers!!))
                            viewAdapter.setItems(BoughtList.getItemList())
                        }.addOnFailureListener {
                            path[i] = localFileList[i].absolutePath
                            Log.d("foto", path[i])
                            BoughtList.modifyItem(Item(item.id.toString(), item.userID, "@drawable/itemicon", item.titolo!!, item.desc!!, item.prezzo!!, item.loc!!, item.exp!!,
                                item.categoria!!, item.isSold, item.isHide, item.interestedUsers!!))
                            viewAdapter.setItems(BoughtList.getItemList())
                        }
                }
            }
        }
    }


    fun setOnListFragmentInteractionListener(callback: OnListFragmentInteractionListener) {
        this.listener = callback
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Item?)
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
            BoughtItemsListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}