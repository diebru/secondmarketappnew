package com.example.seconhandmarketapp.Service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.widget.RemoteViews
import com.example.seconhandmarketapp.Auth
import com.example.seconhandmarketapp.R
import com.example.seconhandmarketapp.Users
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.io.InputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.hours


class MyFirebaseMessagingService: FirebaseMessagingService() {
    private val db= Firebase.firestore
    lateinit var notificationManager : NotificationManager
    lateinit var notificationChannel : NotificationChannel
    lateinit var builder : Notification.Builder
    private val channelId = "com.example.vicky.notificationexample"
    private val description = "Test notification"
    private lateinit var mAuth : FirebaseAuth

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if(remoteMessage.notification != null) {
            Log.d("FCM", "FCM message received!\n" + remoteMessage.data.toString())
        }
        showNotification(remoteMessage)
    }

    private fun showNotification(remoteMessage: RemoteMessage) {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val intent = Intent(this, Auth::class.java)
        val pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)

        val contentView = RemoteViews(packageName, R.layout.notification_layout)
        contentView.setTextViewText(R.id.tv_title, "Second Hand Market APP")
        contentView.setTextViewText(R.id.data, getDate(remoteMessage.sentTime, "kk:mm"))
        contentView.setTextViewText(R.id.tv_content, remoteMessage.notification?.body)
        val bitmap = BitmapFactory.decodeStream(URL(remoteMessage.notification?.imageUrl.toString()).content as InputStream)
        contentView.setImageViewBitmap(R.id.imageNotifica, bitmap)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(channelId,description,NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this,channelId)
                .setCustomContentView(contentView)
                .setSmallIcon(R.drawable.smh_new_2_nosfondo_cropped)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.smh_new_2_nosfondo_cropped))
                .setContentIntent(pendingIntent)
        }else{

            builder = Notification.Builder(this)
                .setContent(contentView)
                .setSmallIcon(R.drawable.smh_new_2_nosfondo_cropped)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.smh_new_2_nosfondo_cropped))
                .setContentIntent(pendingIntent)
        }

        notificationManager.notify(1234,builder.build())
    }

    override fun onNewToken(token: String) {
        Log.d("FCM", "Refreshed token: " + token)
        db.collection("users").document(Users.getUser().toString()).update("token",token)
        db.collection("users").document(Users.getUser().toString()).update("registrationToken",token)
    }

    fun getDate(
        milliSeconds: Long,
        dateFormat: String?
    ): String? { // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat(dateFormat)
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar: Calendar = Calendar.getInstance()
        calendar.setTimeInMillis(milliSeconds)
        return formatter.format(calendar.getTime())
    }
}