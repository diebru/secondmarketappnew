package com.example.seconhandmarketapp

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.os.bundleOf
import com.example.seconhandmarketapp.Users.immaginetemp
import com.example.seconhandmarketapp.Users.logUser
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.log_in.*
import kotlinx.android.synthetic.main.sign_up.*
import kotlinx.android.synthetic.main.sign_up.email_add
import kotlinx.android.synthetic.main.sign_up.full_name
import kotlinx.android.synthetic.main.sign_up.location
import kotlinx.android.synthetic.main.sign_up.nickname
import kotlinx.android.synthetic.main.sign_up.phone
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.URL


val RC_SIGN_IN=101


class Auth : AppCompatActivity() {
    private val TAG = "ahia"
    private lateinit var mAuth : FirebaseAuth
    lateinit var gso: GoogleSignInOptions
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var account: GoogleSignInAccount
    private val db= Firebase.firestore
    private val storage = Firebase.storage
    var storageRef = storage.reference
    private var token: String? = ""
    private val vm: MyViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.host_login)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
        FirebaseApp.initializeApp(this)
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        if(GoogleSignIn.getLastSignedInAccount(this)!=null) {
            account = GoogleSignIn.getLastSignedInAccount(this)!!
        }
        mAuth = FirebaseAuth.getInstance();

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                token = task.result?.token
            })

        vm.fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
        vm.updateCurrentLoc()
    }

    override fun onStart() {
        super.onStart()
        val currentUser: FirebaseUser?
        // Check if user is signed in (non-null) and update UI accordingly.
        if(mAuth.currentUser !=null) {
            currentUser = mAuth.currentUser
            logUser(currentUser!!.uid)
            toMain(currentUser)
        }
    }

    //creo account non con google
    fun createAccount(email: String?, password: String?){
        loadingPanelSignUp.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email!!, password!!)
            .addOnCompleteListener(
                this
            ) { task ->
                if (task.isSuccessful) { // Sign in success, update UI with the signed-in user's information
                   // Log.d(FragmentActivity.TAG, "createUserWithEmail:success")
                    val user = mAuth.currentUser
                    if (user != null) {
                        val docData = hashMapOf(
                            "full_name" to full_name.text.toString(),
                            "nick_name" to nickname.text.toString(),
                            "mail" to user.email,
                            "location" to location.text.toString(),
                            "phone_number" to phone.text.toString(),
                            "coords" to Users.loggedUser?.coords,
                            "token" to token
                        )
                        saveUserImage(user.uid)
                        logUser(user.uid)
                        db.collection("users").document(user.uid) //qui va messo l'effettivo uid
                                .set(docData)
                                .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
                        //Users.loggedUser=User(user.uid, mail=user.email!!)
                        loadingPanelSignUp.setVisibility(View.GONE);


                        toMain(user!!)
                    }
                } else { // If sign in fails, display a message to the user.
                    loadingPanelSignUp.setVisibility(View.GONE);


                    Toast.makeText(
                        this, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }
                // ...
            }
    }

    fun resetPassword(email: String?)
    {
        loadingPanel.setVisibility(View.VISIBLE);
        mAuth.sendPasswordResetEmail(email!!).addOnCompleteListener(
                this
                ) { task ->
            if (task.isSuccessful){
            // Email sent.
                loadingPanel.setVisibility(View.GONE);
                android.app.AlertDialog.Builder(this)
                    .setTitle("EMAIL SENT")
                    .setMessage("An email has been succesfully sent to "+ email)
                    .setPositiveButton("Ok") {
                            dialog: DialogInterface, which: Int -> Toast.makeText(this,"Reset your password and come back",Toast.LENGTH_LONG).show()
                        dialog.cancel()
                    }
                    .create()
                    .show()
                Log.d(TAG, "sendEmail:success")}
            else {
                loadingPanel.setVisibility(View.GONE);
                android.app.AlertDialog.Builder(this)
                    .setTitle("OPERATION FAILED")
                    .setMessage(email+ " is not reachable")
                    .setPositiveButton("Ok") {
                            dialog: DialogInterface, which: Int -> Toast.makeText(this,"Try with another mail",Toast.LENGTH_LONG).show()
                        dialog.cancel()
                    }
                    .create()
                    .show()// If sign in fails, display a message to the user.
                Log.w(
                    TAG,
                    "invio mail fallito",
                    task.exception
                )
                Toast.makeText(
                    this, "Email sending failed.",
                    Toast.LENGTH_SHORT
                ).show()
                updateUI(null)

                // ...
            }
        }

    }



    fun signIn(email: String?, password: String?){
        loadingPanel.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email!!, password!!)
            .addOnCompleteListener(
                this
            ) { task ->
                if (task.isSuccessful) { // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = mAuth.currentUser
                    if (user != null) {
                        logUser(user.uid)
                        loadingPanel.setVisibility(View.GONE);
                        toMain(user)
                        db.collection("users").document(user.uid).update("token",token)
                    } else {
                        Toast.makeText(this, "email o password errate", Toast.LENGTH_LONG)
                            .show()
                    }
                } else { // If sign in fails, display a message to the user.
                    loadingPanel.setVisibility(View.GONE);

                    Log.w(
                        TAG,
                        "signInWithEmail:failure",
                        task.exception
                    )
                    Toast.makeText(
                        this, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)

                    // ...
                }
            }
    }



        fun googleSignIn() {
            val signInIntent = mGoogleSignInClient.signInIntent.also {
                startActivityForResult(it, RC_SIGN_IN)
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)

            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    account = task.getResult(ApiException::class.java)!!
                    Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    // Google Sign In failed, update UI appropriately
                    Log.w(TAG, "Google sign in failed", e)
                    // ...
                }
            }
        }

    private fun firebaseAuthWithGoogle(idToken: String) {

        val credential = GoogleAuthProvider.getCredential(idToken, null)
        loadingPanel.setVisibility(View.VISIBLE);

        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    var exists=false
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = mAuth.currentUser
                    MainScope().launch {

                        withContext(Dispatchers.IO) {
                            if (user != null) {
                                db.collection("users").document(user.uid!!).get()
                                    .addOnSuccessListener { res ->
                                        (if (res.data != null) {
                                            exists = true
                                            logUser(user.uid)
                                            db.collection("users").document(user.uid).update("token",token)
                                            loadingPanel.setVisibility(View.GONE);

                                            toMain(user!!)
                                        } else {
                                            exists = false
                                            createDatas(user)
                                        })


                                    }
                            }
                           /* if(exists)
                            {

                            }
                            else
                            {
                                createDatas(user)
                            }*/
                        }
                    }

                }
                      else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.exception)
                    loadingPanel.setVisibility(View.GONE);

                    Toast.makeText(
                                this, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                            // ...
                            //Snackbar.make(view, "Authentication Failed.", Snackbar.LENGTH_SHORT).show()
                            updateUI(null)
                        }

                    }


        }




    fun createDatas(user : FirebaseUser?){
        Users.registration=true
        var username="username"
        if(account.displayName!=null)
        {username= account.displayName!!
        }
        var phonenumber="phone number"
        var nickname=user!!.email!!.split("@")[0]

        val docData = hashMapOf(
            "full_name" to username,
            "nick_name" to nickname,
            "mail" to user!!.email,
            "location" to "unknown position",
            "phone_number" to phonenumber,
            "coords" to "0.0,0.0",
            "token" to token
        )

        logUser(user!!.uid)
        if(account.photoUrl!=null) {
            Users.googleimage=account.photoUrl!!.toString()
            saveGoogleImage(user!!.uid, account.photoUrl!!.toString())
        }
        Log.d("immaginegoogle", account.photoUrl!!.toString())
        db.collection("users").document(user.uid) //qui va messo l'effettivo uid
            .set(docData)
            .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }

        toMain(user!!)
    }




    fun getUser(){


        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) { // Name, email address, and profile photo Url
            val name = user.displayName
            val email = user.email
            val photoUrl: Uri? = user.photoUrl
            // Check if user's email is verified
            val emailVerified = user.isEmailVerified
            // The user's ID, unique to the Firebase project. Do NOT use this value to
// authenticate with your backend server, if you have one. Use
// FirebaseUser.getIdToken() instead.
            val uid = user.uid
        }
    }

    fun updateUI(user :FirebaseUser?)
    {}


    fun toMain(user:FirebaseUser)
    {
        //logUser(user.uid)
        val intent = Intent(this, MainActivity::class.java).apply {
            putExtra("uid", user.uid)
        }

        startActivity(intent)
    }



    private fun saveGoogleImage(userUid:String, uri:String){
        MainScope().launch {

            withContext(Dispatchers.IO) {
                val bitmap = BitmapFactory.decodeStream(URL(uri).content as InputStream)


                // Toast.makeText(this, file.toString(), Toast.LENGTH_LONG).show()
                val usersRef = storageRef.child("user_images/" + userUid + ".jpg")
                var baos = ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                var data = baos.toByteArray();

                val uploadTask = usersRef.putBytes(data)


// Register observers to listen for when the download is done or if it fails
                uploadTask.addOnFailureListener {
                    // Handle unsuccessful uploads

                }.addOnSuccessListener { Log.d("image", "file caricato") }
            }
        }
    }

    private fun saveUserImage(userUid:String) {

        if (immaginetemp != "@drawable/ic_account") {
            val file: Uri = immaginetemp!!.toUri()


          //  Toast.makeText(this, file.toString(), Toast.LENGTH_LONG).show()
            val usersRef = storageRef.child("user_images/" +userUid + ".jpg")
            val uploadTask = usersRef.putFile(file)

// Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                // Handle unsuccessful uploads


            }.addOnSuccessListener {Log.d("image","file caricato") }

        }
    }

    fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) { //Can add more as per requirement
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                123
            )
        }
    }
}