package com.example.seconhandmarketapp

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import kotlinx.android.synthetic.main.filters_dialog.*
import java.util.*


class FiltersDialogFragment : DialogFragment() {
    private lateinit var listener: NoticeDialogListener
    var fromPrice : String? = null
    var toPrice : String? = null
    var fromExpiry = MutableLiveData<Editable>()
    var toExpiry = MutableLiveData<Editable>()
    var categoria :String? = null
    var location : String? = null
    private val vm: MyViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setother(true)
    }

    interface NoticeDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment)
        fun onDialogNegativeClick(dialog: DialogFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.filters_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonOk.setOnClickListener {
            fromPrice = editFromPrice.text.toString()
            toPrice = editToPrice.text.toString()
            location = editLocation.text.toString()
            Log.d("filter", fromExpiry.value.toString())
            listener.onDialogPositiveClick(this)
            this.dismiss()
        }
        buttonReset.setOnClickListener{
            listener.onDialogNegativeClick(this)
            this.dismiss()
        }
        initialize()
        datePicker()
        spinner()
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ConstraintLayout.LayoutParams.MATCH_PARENT
        params.height = ConstraintLayout.LayoutParams.MATCH_PARENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
        dialog!!.window!!.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));
    }

    private fun initialize(){
        fromExpiry.observe((context as AppCompatActivity), androidx.lifecycle.Observer { editFromExpiry.text = it })
        toExpiry.observe((context as AppCompatActivity), androidx.lifecycle.Observer { editToExpiry.text = it })
        button_close.setOnClickListener {
            dialog?.dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d("dialgo","onAttach")
        try {
            listener = targetFragment as NoticeDialogListener
            Log.d("dialgo","listener")
        } catch (e: ClassCastException){
            throw java.lang.ClassCastException((context.toString() + "must implement NoticeDialogListener"))
        }
    }

    private fun datePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)+1
        val day = c.get(Calendar.DAY_OF_MONTH)

        editFromExpiry.setOnClickListener {
            Log.w("xxx","dentro")

            val dpd = DatePickerDialog (context as AppCompatActivity, DatePickerDialog.OnDateSetListener{
                    view, mYear, mMonth, mDay -> fromExpiry.value = SpannableStringBuilder("$mDay/$mMonth/$mYear")
            }, year, month, day)

            dpd.show()
        }
        editToExpiry.setOnClickListener {
            Log.w("xxx","dentro")

            val dpd = DatePickerDialog (context as AppCompatActivity, DatePickerDialog.OnDateSetListener{
                    view, mYear, mMonth, mDay -> toExpiry.value = SpannableStringBuilder("$mDay/$mMonth/$mYear")
            }, year, month, day)

            dpd.show()
        }
    }

    private fun spinner() {
        spinnerCategory.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long){
                // Display the selected item text on text view
                if (position != 0)
                    categoria = parent.getItemAtPosition(position).toString()
            }



            override fun onNothingSelected(parent: AdapterView<*>){
                //Nothing
            }
        }

    }

}