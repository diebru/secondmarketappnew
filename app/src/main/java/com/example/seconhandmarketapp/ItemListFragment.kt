package com.example.seconhandmarketapp

import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.seconhandmarketapp.ItemList.modifyItem
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.fragment_item_list.*
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import kotlinx.coroutines.*
import java.io.File


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ItemListFragment.OnListFragmentInteractionListener] interface.
 */

class ItemListFragment : Fragment() {

    private val db = Firebase.firestore
    private var listener: ItemListFragment.OnListFragmentInteractionListener? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyItemRecyclerViewAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var l = listOf<Item>()
    private var TAG = "users"
    private var first = false
    private var columnCount = 1
    private val storage = Firebase.storage
    var storageRef = storage.reference
    private var new = false
    private val vm: MyViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setItemList()
    }

    override fun onResume() {
        super.onResume()
        vm.setItemList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR;

        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        viewManager = LinearLayoutManager(context)
        viewAdapter = MyItemRecyclerViewAdapter(l, listener, null, null, null, false, false, false)
        // Set the adapter
        recyclerView = view.findViewById<RecyclerView>(R.id.list).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        if(arguments?.getBoolean("new") != null)
            new = arguments?.getBoolean("new")!!

        if(new) {
            ItemList.clear()
            realtimeUpdateDB()
        } else {
            if (ItemList.isEmpty())
                realtimeUpdateDB()
            else {
                viewAdapter.setItems(ItemList.getItemList())
                view.noItemLabel.text = ""
            }
        }

        val fab: FloatingActionButton = view.fab_list
        fab.setOnClickListener {
            //sto creando un nuovo elemento, incremento l'id e lo passo alla edit activity
            val b = Bundle()
            b.putInt("item", ++counter)
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
            findNavController().navigate(R.id.action_itemFragment_to_itemEditFragment, b)
        }

        view.swipe_layout.isEnabled=false

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */

    fun setOnListFragmentInteractionListener(callback: OnListFragmentInteractionListener) {
        this.listener = callback
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Item?)
        fun onEditFragmentInteraction(item: Item?)
    }

    private fun realtimeUpdateDB() {
        db.collection("users").document(Users.getUser()!!).collection("items").get()
            .addOnSuccessListener {  res ->
                val d = res.documents
                ItemList.clear()
                for (item in d) {
                    ItemList.addItem(
                        Item(
                            item.data?.get("itemID").toString(),
                            item.data?.get("userID").toString(),
                            "@drawable/itemicon",
                            item.data?.get("title").toString(),
                            item.data?.get("description").toString(),
                            item.data?.get("price").toString(),
                            item.data?.get("location").toString(),
                            item.data?.get("expirydate").toString(),
                            item.data?.get("category").toString(),
                            item.data?.get("isSold") as Boolean,
                            item.data!!["isHide"] as Boolean,
                            item.data!!["interestedUsers"] as ArrayList<String>
                        )
                    )
                    Log.d(TAG, "${item.id} => ${item.data}")
                }
                viewAdapter.setItems(ItemList.getItemList())
                downloadPhoto()
                if(!first) {
                    if (!ItemList.isEmpty())
                        noItemLabel.text = ""
                    else
                        noItemLabel.text = getString(R.string.noitem)
                    first = true
                }
                Log.d(TAG, "Listen succeed")
            }
    }

    private fun downloadPhoto() {
        val localFileList: MutableList<File> = arrayListOf()
        val path: MutableList<String> = arrayListOf()
        for ((i, item) in ItemList.list.withIndex()) {
            MainScope().launch {
                path.add("@drawable/itemicon")
                localFileList.add(File.createTempFile("image" + i, "jpg"))
                withContext(Dispatchers.IO) {
                    storageRef.child("user_images/" + item.userID + "/" + item.id + ".jpg")
                        .getFile(localFileList[i])
                        .addOnSuccessListener {
                            path[i] = localFileList[i].absolutePath
                            Log.d("foto", path[i])
                            modifyItem(Item(item.id.toString(), item.userID, path[i], item.titolo!!, item.desc!!, item.prezzo!!, item.loc!!, item.exp!!,
                                item.categoria!!, item.isSold, item.isHide, item.interestedUsers!!))
                            viewAdapter.setItems(ItemList.getItemList())
                        }
                }
            }
        }
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
            ItemListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}