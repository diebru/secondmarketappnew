package com.example.seconhandmarketapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.seconhandmarketapp.ItemListFragment.OnListFragmentInteractionListener
import com.example.seconhandmarketapp.dummy.DummyContent.DummyItem
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.fragment_item.view.*
import kotlinx.android.synthetic.main.fragment_item.view.image
import kotlinx.android.synthetic.main.fragment_item.view.price
import kotlinx.android.synthetic.main.fragment_item.view.title
import kotlinx.android.synthetic.main.fragment_item_list.*
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import java.util.regex.Pattern

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class  MyItemRecyclerViewAdapter(
    private var mValues: List<Item>,
    private var mListener: ItemListFragment.OnListFragmentInteractionListener?,
    private var mOnSaleListener: OnSaleListFragment.OnListFragmentInteractionListener?,
    private var mInterestListener: ItemOfInterestListFragment.OnListFragmentInteractionListener?,
    private var mBoughtListener: BoughtItemsListFragment.OnListFragmentInteractionListener?,
    private val isOnSaleList: Boolean,
    private val isInterestList: Boolean,
    private val isBoughtList: Boolean
) : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>(),
    Filterable{
    private var TAG = "filter"
    private val mOnClickListener: View.OnClickListener
    var onSearchList : Boolean = false
    var searchableList: MutableList<Item> = arrayListOf()
    private val l = ReentrantLock()
    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Item
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.

            if (isOnSaleList)
                mOnSaleListener?.onListFragmentInteraction(item)
            else if (isInterestList)
                mInterestListener?.onListFragmentInteraction(item)
            else if (isBoughtList)
                mBoughtListener?.onListFragmentInteraction(item)
            else
                mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = mValues.size

    fun setItems(newItems :List<Item>){
        val diffs = DiffUtil.calculateDiff(ItemDiffCallback(mValues, newItems))
        mValues = newItems
        diffs.dispatchUpdatesTo(this)
    }


    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val imgView : ImageView = mView.image
        val tileView: TextView = mView.title
        val priceView: TextView = mView.price
        val expView: TextView = mView.exp
        val locView: TextView = mView.loc
        val editbutton: MaterialButton = mView.editbutton
        val bInvisible: ImageView = mView.bInvisible
        val bVisible: ImageView = mView.bVisible
        val im_soldOut: ImageView = mView.im_soldout

        fun bind(card: Item) {
            if(card.img!="@drawable/itemicon")
                imgView.setImageURI(card.img?.toUri())
            tileView.text = card.titolo
            priceView.text = (card.prezzo + " €")
            expView.text = card.exp
            locView.text = card.loc

            with(mView) {
                tag = card
                setOnClickListener(mOnClickListener)
            }
            Log.d("user", "card: ${card.userID} user: ${Users.getUser()}")
            if(card.userID == Users.getUser()) {
                if (!card.isSold)    im_soldOut.visibility = View.GONE
                if(!card.isHide)     bInvisible.visibility = View.GONE
                else                bVisible.visibility = View.GONE
                with(editbutton) {
                    tag = card
                    setOnClickListener { v ->
                        val item = v.tag as Item
                        // Notify the active callbacks interface (the activity, if the fragment is attached to
                        // one) that an item has been selected.
                        mListener?.onEditFragmentInteraction(item)
                    }
                }
            }
            else {
                editbutton.visibility = View.GONE
                im_soldOut.visibility = View.GONE
                bInvisible.visibility = View.GONE
                bVisible.visibility = View.GONE
            }

        }
        override fun toString(): String {
            return ""//super.toString() + " '" + mContentView.text + "'"
        }

    }

    inner class ItemDiffCallback(private val oldItems: List<Item>, private val newItems: List<Item>): DiffUtil.Callback(){
        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition].id == newItems[newItemPosition].id
        }

        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            val (_, img,titolo,desc,prezzo,categoria,loc,exp) = oldItems[oldPosition]
            val (_, img1,titolo1,desc1,prezzo1,categoria1,loc1,exp1) = newItems[newPosition]
            Log.d("updateImg","${img} - ${img1}")
            return img == img1 && titolo == titolo1 && desc==desc1 && prezzo==prezzo1 && categoria == categoria1 && loc == loc1 && exp==exp1
        }

    }

    override fun getFilter(): Filter {
        return  object : Filter() {
            private val filterResults = FilterResults()

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                var list = OnSaleList.getItemList()
                var tmpList = mutableListOf<Item>()
                Log.d("lista", "${onSearchList}")

                if(l.tryLock(1000, TimeUnit.MILLISECONDS)){
                    try {
                        if(!onSearchList)
                            searchableList.clear()
                        else
                            list = searchableList
                        if (constraint.isNullOrBlank()) {
                            searchableList.addAll(OnSaleList.getItemList())
                        }
                        // filtro per prezzo
                        else if(constraint.contains(":price\\")){
                            val p = Pattern.compile("-?\\d+(\\.\\d+)?")
                            var values = arrayOfNulls<Double>(2)
//                    Log.d("match", "${constraint}")
                            var matcher = p.matcher(constraint.toString())
//                    Log.d("match", "${matcher.groupCount()}")
                            var i = 0
                            while(matcher.find()) {
//                        Log.d("match", matcher.group())
                                values[i++] = matcher.group().toDouble()
                            }
                            for (item in list) {
                                if(item.prezzo!!.matches("-?\\d+(\\.\\d+)?".toRegex())) {
                                    if (item.prezzo.toDouble() >= values[0]!! &&
                                        item.prezzo.toDouble() <= values[1]!!)
                                        tmpList.add(item)
                                }
                            }
                            onSearchList = true
                        }
                        //filtro per location
                        else if (constraint.contains(":location\\")){
                            val filterPattern = constraint.split("\\")[1].toLowerCase().trim { it <= ' ' }
                            Log.d(TAG,filterPattern)
                            for (item in list) {
                                if (item.loc?.toLowerCase() == filterPattern)
                                    tmpList.add(item)
                            }
                            onSearchList = true
                        }
                        //filtro per categoria
                        else if (constraint.contains(":category\\")) {
                            val c = constraint.split("\\")[1]
//                    Log.d(TAG, c)
                            for (item in list) {
                                if (item.categoria == c)
                                    tmpList.add(item)
                            }
                            onSearchList = true

                        }
                        //filtro per expiry date
                        else if (constraint.contains(":expirydate\\")) {
//                    Log.d(TAG, constraint.toString())
                            val df = DateFormat.getDateInstance(DateFormat.SHORT)
                            val values = constraint.split("\\")[1].split("to")
                            val from  = df.parse(values[0])
                            val to = df.parse(values[1])
                            Log.d(TAG,from.toString() + to.toString())
                            if(from <  to){
                                for (item in list) {
                                    val tmpExp = df.parse(item.exp!!)
                                    if (tmpExp >= from && tmpExp <= to)
                                        tmpList.add(item)
                                }
                                onSearchList = true
                            }
                        }
                        // ricerca per titolo
                        else{
                            val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
                            for (item in 0..list.size) {
                                if (list[item].titolo?.toLowerCase(Locale.ROOT)?.contains(filterPattern)!!) {
                                    tmpList.add(list[item])
                                }
                            }
                        }
                    }
                    finally {
                        searchableList = tmpList
                        Log.d("aggiornato", "dentro")
                        l.unlock()
                    }
                }

                return filterResults.also {
                    it.values = searchableList
                }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
//                if (searchableList.isNullOrEmpty())
//                    onNothingFound?.invoke()
                setItems(searchableList)

            }
        }
    }

}
