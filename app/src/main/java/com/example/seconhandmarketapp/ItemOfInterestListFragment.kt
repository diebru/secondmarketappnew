package com.example.seconhandmarketapp


import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_item_list.*
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.RuntimeException


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ItemOfInterestListFragment.OnListFragmentInteractionListener] interface.
 */


class ItemOfInterestListFragment : Fragment() {
    private val db = Firebase.firestore
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyItemRecyclerViewAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var l = listOf<Item>()
    private var TAG = "users"
    private var first = false
    private val storage = Firebase.storage
    var storageRef = storage.reference
    private val vm: MyViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setInterestList()
    }

    override fun onResume() {
        super.onResume()
        vm.setInterestList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR

        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        viewManager = LinearLayoutManager(context)
        viewAdapter = MyItemRecyclerViewAdapter(l, null, null, listener, null, false, true, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.list).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        if(ItemOfInterestList.update) {
            ItemOfInterestList.clear()
            realtimeUpdateDB()
        } else {
            if (ItemOfInterestList.isEmpty())
                realtimeUpdateDB()
            else {
                viewAdapter.setItems(ItemOfInterestList.getItemList())
                view.noItemLabel.text = ""
            }
        }

        var fab: FloatingActionButton = view.fab_list
        fab.visibility = View.GONE
        fab = view.fab_refresh
        fab.visibility = View.GONE

        view.swipe_layout.isEnabled=false

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ItemOfInterestListFragment.OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun realtimeUpdateDB() {
        db.collectionGroup("items").addSnapshotListener { documents, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed", e)
                    return@addSnapshotListener
                }
                ItemOfInterestList.clear()
                ItemOfInterestList.setUpd(false)
                for (item in documents!!) {
                    val users = item.data["interestedUsers"] as ArrayList<String>
                    if(item.data["userID"] != Users.getUser() && item.data["isSold"] != true) {
                        for (member in users) {
                            if (member == Users.getUser()) {
                                ItemOfInterestList.addItem(
                                    Item(
                                        item.data["itemID"].toString(),
                                        item.data["userID"].toString(),
                                        "@drawable/itemicon",
                                        item.data["title"].toString(),
                                        item.data["description"].toString(),
                                        item.data["price"].toString(),
                                        item.data["location"].toString(),
                                        item.data["expirydate"].toString(),
                                        item.data["category"].toString(),
                                        item.data["isSold"] as Boolean,
                                        item.data["isHide"] as Boolean,
                                        item.data["interestedUsers"] as ArrayList<String>
                                    )
                                )
                                Log.d(TAG, "${item.id} => ${item.data}")
                            }
                        }
                    }
                }
                viewAdapter.setItems(ItemOfInterestList.getItemList())
                downloadPhoto()
                if(!first) {
                    if (!ItemOfInterestList.isEmpty())
                        noItemLabel.text = ""
                    else
                        noItemLabel.text = "No item to show"
                    first = true
                }
                Log.d(TAG, "Listen succeed")
            }
    }

    private fun downloadPhoto() {
        val localFileList: MutableList<File> = arrayListOf()
        val path: MutableList<String> = arrayListOf()
        for ((i, item) in ItemOfInterestList.list.withIndex()) {
            MainScope().launch {
                path.add("@drawable/itemicon")
                localFileList.add(File.createTempFile("image" + i, "jpg"))
                withContext(Dispatchers.IO) {
                    storageRef.child("user_images/" + item.userID + "/" + item.id + ".jpg")
                        .getFile(localFileList[i])
                        .addOnSuccessListener {
                            path[i] = localFileList[i].absolutePath
                            Log.d("foto", path[i])
                            ItemOfInterestList.modifyItem(Item(item.id.toString(), item.userID, path[i], item.titolo!!, item.desc!!, item.prezzo!!, item.loc!!, item.exp!!,
                                item.categoria!!, item.isSold, item.isHide, item.interestedUsers))
                            viewAdapter.setItems(ItemOfInterestList.getItemList())
                        }
                }
            }
        }
    }

    fun setOnListFragmentInteractionListener(callback: OnListFragmentInteractionListener) {
        this.listener = callback
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Item?)
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
            ItemOfInterestListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}