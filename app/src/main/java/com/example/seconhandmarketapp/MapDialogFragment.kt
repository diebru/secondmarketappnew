package com.example.seconhandmarketapp

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.Gson
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import kotlinx.android.synthetic.main.map_dialog.*
import kotlinx.android.synthetic.main.map_dialog.view.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_LOC = "loc"
private const val ARG_TYPE = "type"

enum class Action(val value: Int){
    SHOW(0),
    SHOW_BUYABLE_ITEM(1),
    EDIT(2);

    companion object {
        fun from(i: Int): Action? = values().find { it.value == i }
    }
}

/**
 * A simple [Fragment] subclass.
 * Use the [MapDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MapDialogFragment : DialogFragment(), OnMapReadyCallback {
    internal lateinit var listener: MapDialogListener
    lateinit var loc: LatLng
    private lateinit var type: Action
    private lateinit var currentLoc: LatLng

    private lateinit var mapView : MapView
    lateinit var gMap: GoogleMap
    lateinit var route : Polyline
    private val vm: MyViewModel by activityViewModels()

    interface MapDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setother(true)

        vm.updateCurrentLoc()
        currentLoc = vm.currentLoc

        if(savedInstanceState!=null)
        {
            type = Action.from(savedInstanceState.getInt("type"))!!

            loc = LatLng(
                savedInstanceState.getFloatArray(ARG_LOC)?.get(0)?.toDouble()!!,
                savedInstanceState.getFloatArray(ARG_LOC)?.get(1)?.toDouble()!!
            )
        }
        else {
            if (arguments != null)
                arguments?.let {
                    loc = LatLng(
                        it.getFloatArray(ARG_LOC)?.get(0)?.toDouble()!!,
                        it.getFloatArray(ARG_LOC)?.get(1)?.toDouble()!!
                    )

                    type = Action.from(it.getInt(ARG_TYPE))!!
                }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.map_dialog, container, false)
        mapView= v.map
        mapView.onCreate(bundleOf())
        mapView.getMapAsync(this);
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(type==Action.EDIT) {
            Okbutton.isVisible = false
            Cancelbutton.setOnClickListener{
                this.dismiss()
            }
            Confirmbutton.setOnClickListener {
                listener.onDialogPositiveClick(this)
                this.dismiss()
            }
        }
        else
        {
            Confirmbutton.isVisible=false
            Cancelbutton.isVisible=false
            Okbutton.setOnClickListener{
                this.dismiss()
            }
        }
        if(type!=Action.SHOW_BUYABLE_ITEM)
            path.isVisible=false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(bundleOf())
        outState.putFloatArray("loc", floatArrayOf(loc.latitude.toFloat(), loc.longitude.toFloat()))
        outState.putInt("type", type.value)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BlankFragment.
         */
        @JvmStatic
        fun newInstance(loc: LatLng, action: Action=Action.SHOW) =
            MapDialogFragment().apply {
                arguments = Bundle().apply {
                    putFloatArray(ARG_LOC, floatArrayOf(loc.latitude.toFloat(),
                        loc.longitude.toFloat()
                    ))
                    putInt(ARG_TYPE, action.value)
                }
            }
    }

    override fun onMapReady(googleMap: GoogleMap) { // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        gMap = googleMap

        googleMap.addMarker(
            MarkerOptions().position(loc)
                .title("Location")
        )

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 15.0F))

        if(type==Action.EDIT)
            googleMap.setOnMapLongClickListener { coords: LatLng ->
                googleMap.clear()
                googleMap.addMarker(
                    MarkerOptions().position(coords)
                    .title("Location")
                )
                loc=coords
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(coords))
            }
        else if (type==Action.SHOW_BUYABLE_ITEM) {
            val start: MarkerOptions =
                MarkerOptions().position(Users.loggedUser?.coords!!).title("You")
            val dest: MarkerOptions = MarkerOptions().position(loc).title("Destination")
            /*
            val url: String = getDirectionURL(start.position, dest.position)
            GetDirection(url).execute()
            */
            path.setOnClickListener {
                googleMap.clear()
                googleMap.addMarker(dest)
                if(path.text==getString(R.string.showDirection)) {
                    googleMap.addMarker(start)
                    googleMap.addPolyline(
                        PolylineOptions().add(
                            start.position,
                            dest.position
                        ).width(12F).color(Color.GREEN).geodesic(true)
                    )
                    path.text=getString(R.string.hideDirection)
                }
                else
                    path.text=getString(R.string.showDirection)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ConstraintLayout.LayoutParams.MATCH_PARENT
        params.height = ConstraintLayout.LayoutParams.MATCH_PARENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
        dialog!!.window!!.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d("dialog","onAttach")
        try {
            if(Action.from(requireArguments().getInt(ARG_TYPE))==Action.EDIT)
            {
                listener = targetFragment as MapDialogListener
                Log.d("dialog","listener")
            }
        } catch (e: ClassCastException){
            throw java.lang.ClassCastException((context.toString() + "must implement NoticeDialogListener"))
        }
    }

    fun getDirectionURL(origin:LatLng,dest:LatLng) : String{
        return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}&destination=${dest.latitude},${dest.longitude}&sensor=false&mode=driving&key=AIzaSyAYSMJ9MLWfF_1b9eD6_6OtKCPnH2Gv34Q"//+getString(R.string.google_maps_key)
    }

    private inner class GetDirection(val url : String) : AsyncTask<Void, Void, List<List<LatLng>>>(){
        override fun doInBackground(vararg params: Void?): List<List<LatLng>> {
            val client = OkHttpClient()
            val request = Request.Builder().url(url).build()
            val response = client.newCall(request).execute()
            val data = response.body()!!.string()
            Log.d("GoogleMap" , " data : $data")
            val result =  ArrayList<List<LatLng>>()
            try{
                val respObj = Gson().fromJson(data,GoogleMapDTO::class.java)

                val path =  ArrayList<LatLng>()

                for (i in 0..(respObj.routes[0].legs[0].steps.size-1)){
//                    val startLatLng = LatLng(respObj.routes[0].legs[0].steps[i].start_location.lat.toDouble()
//                            ,respObj.routes[0].legs[0].steps[i].start_location.lng.toDouble())
//                    path.add(startLatLng)
//                    val endLatLng = LatLng(respObj.routes[0].legs[0].steps[i].end_location.lat.toDouble()
//                            ,respObj.routes[0].legs[0].steps[i].end_location.lng.toDouble())
                    path.addAll(decodePolyline(respObj.routes[0].legs[0].steps[i].polyline.points))
                }
                result.add(path)
            }catch (e:Exception){
                e.printStackTrace()
            }
            return result
        }

        override fun onPostExecute(result: List<List<LatLng>>) {
            val lineoption = PolylineOptions()
            for (i in result.indices){
                lineoption.addAll(result[i])
                lineoption.width(10f)
                lineoption.color(Color.BLUE)
                lineoption.geodesic(true)
            }
            route = gMap.addPolyline(lineoption)
            route.isVisible=false
        }
    }

    public fun decodePolyline(encoded: String): List<LatLng> {

        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val latLng = LatLng((lat.toDouble() / 1E5),(lng.toDouble() / 1E5))
            poly.add(latLng)
        }

        return poly
    }
}

