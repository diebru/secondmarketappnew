package com.example.seconhandmarketapp

import com.google.android.gms.maps.model.LatLng

object ItemList {
    var notEmpty = true
    var first = true
    val list = mutableListOf<Item>()

    fun getItem(itemId: Int) = list.find { i -> i.id == itemId }
    fun addItem(item: Item) = list.add(item)
    fun addItemList(addedlist: List<Item>) = list.addAll(addedlist)
    fun modifyItem(item: Item) = list.set(list.indexOf(getItem(item.id)), item)
    fun getItemList(): List<Item> = list.toList()
    fun clear() = list.clear()
    fun isEmpty() = list.isEmpty()
    fun getSize() = list.size
}

object BoughtList {
    var notEmpty = true
    var first = true
    var update = true
    val list = mutableListOf<Item>()

    fun getItem(itemId: Int) = list.find { i -> i.id == itemId }
    fun addItem(item: Item) = list.add(item)
    fun addItemList(addedlist: List<Item>) = list.addAll(addedlist)
    fun modifyItem(item: Item) {
        for((i, currItem) in BoughtList.list.withIndex()) {
            if(currItem.id == item.id && currItem.userID == item.userID)
                BoughtList.list[i] = item
        }
    }
    fun getItemList(): List<Item> = list.toList()
    fun clear() = list.clear()
    fun isEmpty() = list.isEmpty()
    fun getSize() = list.size
    fun setUpd(b : Boolean) {update = b}
}

object ItemOfInterestList {
    var notEmpty = true
    var first = true
    var update = true
    val list = mutableListOf<Item>()

    fun getItem(itemId: Int) = list.find { i -> i.id == itemId }
    fun addItem(item: Item) = list.add(item)
    fun addItemList(addedlist: List<Item>) = list.addAll(addedlist)
    fun modifyItem(item: Item) {
        for((i, currItem) in list.withIndex()) {
            if(currItem.id == item.id && currItem.userID == item.userID)
                list[i] = item
        }
    }
    fun getItemList(): List<Item> = list.toList()
    fun clear() = list.clear()
    fun isEmpty() = list.isEmpty()
    fun getSize() = list.size
    fun setUpd(b : Boolean) {update = b}
}

object OnSaleList {
    var first = true
    val list = mutableListOf<Item>()

    fun getItem(itemId: Int) = list.find { i -> i.id == itemId }
    fun addItem(item: Item) = list.add(item)
    fun addItemList(addedlist: List<Item>) = list.addAll(addedlist)
    fun modifyItem(item: Item) {
        for((i, currItem) in list.withIndex()) {
            if(currItem.id == item.id && currItem.userID == item.userID)
                list[i] = item
        }
    }
    fun getItemList(): List<Item> = list.toList()
    fun clear() = list.clear()
    fun isEmpty() = list.isEmpty()
    fun size() = list.size
}

var counter = 0

data class Item(
    val id: Int,
    var img: String?,
    val titolo: String?,
    val desc: String?,
    val prezzo: String?,
    val categoria: String?,
    val loc: String?,
    val exp: String?,
    var coords: LatLng = LatLng(0.0,0.0)
) {
    var userID: String = ""
    var isSold: Boolean = false
    var isHide: Boolean = false
    var interestedUsers: ArrayList<String> = arrayListOf()

    constructor(
        itemID: String,
        userID: String,
        img: String,
        title: String,
        description: String,
        price: String,
        location: String,
        expirydate: String,
        category: String,
        isSold: Boolean,
        isHide: Boolean,
        interestedUsers: ArrayList<String>,
        coords: LatLng= LatLng(0.0,0.0)
    ) : this(
        itemID.toInt(),
        img,
        title,
        description,
        price,
        category,
        location,
        expirydate,
        coords
    ){
        this.userID = userID
        this.isHide = isHide
        this.isSold = isSold
        this.interestedUsers = interestedUsers
    }
}

