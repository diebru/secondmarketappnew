package com.example.seconhandmarketapp

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import android.Manifest
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.example.seconhandmarketapp.Users.googleimage
import com.example.seconhandmarketapp.Users.immaginetemp
import com.example.seconhandmarketapp.Users.registration
import com.example.seconhandmarketapp.Users.riempiUser
import com.example.seconhandmarketapp.Users.setUserImage
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.nav_header_main.*
import java.io.File


class MainActivity : AppCompatActivity(), ItemListFragment.OnListFragmentInteractionListener, OnSaleListFragment.OnListFragmentInteractionListener,
                                        BoughtItemsListFragment.OnListFragmentInteractionListener, ItemOfInterestListFragment.OnListFragmentInteractionListener,
                                        NavigationView.OnNavigationItemSelectedListener {
    private var TITLES_KEY = "TITLES_KEY"
    private var DESCS_KEY = "DESCS_KEY"
    private var PRICES_KEY = "PRICES_KEY"
    private var CATEGORIES_KEY = "CATEGORIES_KEY"
    private var LOCS_KEY = "LOCS_KEY"
    private var EXPS_KEY = "EXPS_KEY"
    val db= Firebase.firestore
    val storage = Firebase.storage
    var storageRef = storage.reference
    private val vm: MyViewModel by viewModels()
    lateinit var uid : String
    private lateinit var navController: NavController
    private lateinit var drawerLayout: DrawerLayout


    private lateinit var appBarConfiguration: AppBarConfiguration
    // intero per creare dinamicamente degli id cresenti

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)

        // imposto l'immagine profilo e nickname
        val headView = navView.getHeaderView(0)
        val filename = getString(R.string.profile_pic_key)
        val file = File(this.filesDir, filename)
        if (file.exists()) {
            val image = headView.findViewById<ImageView>(R.id.image_preview)
            image.setImageURI(file.toUri())
        }
        val nkn = headView.findViewById<TextView>(R.id.nkn_preview)

        val bundle = intent.extras
        if(bundle!=null)
        {
            if(bundle.getString("uid")!=null)
            {uid=bundle.getString("uid").toString()
                db.collection("users").document(uid!!) //qui va messo l'effettivo uid
                    .get()
                    .addOnSuccessListener { res ->
                        riempiUser(
                            res.getString("full_name")!!,
                            res.getString("nick_name")!!,
                            res.getString("mail")!!,
                            res.getString("phone_number")!!,
                            res.getString("location")!!,
                            coords =  if(res.getString("coords")!=null) {
                                val l = res.getString("coords").toString().split(",")
                                LatLng(l[0].toDouble(), l[1].toDouble())
                            }  else vm.currentLoc
                        )
                        nkn.text=Users.getUserData()?.nick
                    }
                if(!registration)   downloadPhoto()
            }
        }

        navController = findNavController(R.id.nav_host_fragment)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.onSaleListFragment, R.id.itemListFragment, R.id.itemOfInterestListFragment, R.id.boughtItemsListFragment, R.id.showProfileFragment, R.id.log_out), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.setNavigationItemSelectedListener(this)

        vm.fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
        vm.updateCurrentLoc()
        if(Users.registration){
            val image = headView.findViewById<ImageView>(R.id.image_preview)
            image.setImageURI(googleimage.toUri())
            Glide.with(this)
                .load(Users.googleimage)
                .into(image)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.log_out) {
            db.collection("users").document(Users.getUser().toString()).update("token","")
            db.collection("users").document(Users.getUser().toString()).update("registrationToken","")
            val mAuth = FirebaseAuth.getInstance();
            if(mAuth!= null)
            {
                mAuth.signOut()
                val intent = Intent(this, Auth::class.java).apply {
                }
                Users.loggedUser=null
                Users.immaginetemp=""
                ItemList.clear()
                OnSaleList.clear()
                ItemOfInterestList.clear()
                BoughtList.clear()
                startActivity(intent)
                Users.registration = false
            }
        }
        else {
            when (item.itemId) {
                R.id.onSale -> navController.navigate(R.id.onSaleListFragment)
                R.id.myOffers -> navController.navigate(R.id.itemListFragment)
                R.id.wishList -> navController.navigate(R.id.itemOfInterestListFragment)
                R.id.myPurchases -> navController.navigate(R.id.boughtItemsListFragment)
                R.id.myProfile -> navController.navigate(R.id.showProfileFragment)
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun downloadPhoto() {
        val localFile = File.createTempFile("image", "jpg")
        storageRef.child("user_images/" + uid + ".jpg").getFile(localFile).addOnSuccessListener {
            // Local temp file has been created
            if (localFile != null) {
                //image.setImageURI(localFile.absolutePath.toUri())
                if (!registration) {
                    immaginetemp = localFile.absolutePath
                    setUserImage(localFile.absolutePath)

                    var imgPreview = findViewById<ImageView>(R.id.image_preview)
                    imgPreview.setImageURI(localFile.absolutePath.toUri())
                }
            }
        }.addOnFailureListener {
            // Handle any errors
            //l'immagine ancora non è stata cambiata
            if (immaginetemp == "") {
                immaginetemp = "@drawable/ic_account"
                setUserImage("@drawable/ic_account")
                var imgPreview = findViewById<ImageView>(R.id.image_preview)
                imgPreview.setImageResource(R.drawable.ic_account)
            } else {
                setUserImage(immaginetemp)
                var imgPreview = findViewById<ImageView>(R.id.image_preview)
                imgPreview.setImageURI(immaginetemp.toUri())
            }

        }
    }
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is ItemListFragment) {
            fragment.setOnListFragmentInteractionListener(this)
        }
        else if (fragment is OnSaleListFragment) {
            fragment.setOnListFragmentInteractionListener(this)
        }
        else if (fragment is ItemOfInterestListFragment) {
            fragment.setOnListFragmentInteractionListener(this)
        }
        else if (fragment is BoughtItemsListFragment) {
            fragment.setOnListFragmentInteractionListener(this)
        }
    }

    override fun onListFragmentInteraction(item: Item?){
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
        Log.d("item", "${item?.titolo}")
        val b = Bundle()
        b.putString("itemID", item?.id.toString())
        b.putString("userID", item?.userID)
        b.putString("img", item?.img)

        if(vm.fragmentItemList)
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_itemFragment_to_itemDetailsFragment, b)
        if(vm.fragmentOnSale)
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_onSaleListFragment_to_itemDetailsFragment, b)
        if(vm.fragmentInterestList)
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_itemOfInterestListFragment_to_itemDetailsFragment, b)
        if(vm.fragmentBought){
            b.putBoolean("buyer", true)
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_boughtItemsListFragment_to_itemDetailsFragment, b)
        }

    }

    override fun onEditFragmentInteraction(item: Item?){
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
        val b = Bundle()
        b.putString("itemID", item?.id.toString())
        b.putString("userID", item?.userID)
        b.putString("img", item?.img)
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_itemFragment_to_itemEditFragment, b)
    }

    override fun onBackPressed() {
        if(vm.other) {
            vm.setother(false)
            super.onBackPressed()
        }
        else if(vm.fragmentProfile) {
            if (Users.loggedUser != null)   super.onBackPressed()
            else    navController.navigate(R.id.onSaleListFragment)
        }
        else if(vm.fragmentItemList || vm.fragmentInterestList || vm.fragmentBought)
            navController.navigate(R.id.onSaleListFragment)
        else if(vm.fragmentOnSale) {
            if(!vm.close) {
                Toast.makeText(this, R.string.closeapp, Toast.LENGTH_LONG).show()
                vm.setClose()
            }
            else {
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
            }
        }
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) { //Can add more as per requirement
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                123
            )
        }
    }
}