package com.example.seconhandmarketapp

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.location.Address
import android.location.Geocoder
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Debug
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.*
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.seconhandmarketapp.Users.getUser
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.navigation.NavigationView
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.io.File
import java.io.IOException
import java.io.InputStream
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_edit_profile.full_name
import kotlinx.android.synthetic.main.fragment_edit_profile.image
import kotlinx.android.synthetic.main.fragment_edit_profile.location
import kotlinx.android.synthetic.main.fragment_edit_profile.nickname
import kotlinx.android.synthetic.main.fragment_edit_profile.phone
import kotlinx.android.synthetic.main.sign_up.*
import java.security.Timestamp
import java.util.*

class EditProfileFragment : Fragment(), MapDialogFragment.MapDialogListener {
    private var img = MutableLiveData<String>()
    private var currentPhotoUri = MutableLiveData<Uri>()
    private var name = MutableLiveData<Editable>()
    private var nkn = MutableLiveData<Editable>()
    private var mail = MutableLiveData<Editable>()
    private var tel = MutableLiveData<Editable>()
    private var loc = MutableLiveData<Editable>()
    lateinit var coords : LatLng
    private var fromCamera = MutableLiveData<Boolean>()
    private var fromGallery = MutableLiveData<Boolean>()
    //private var itemId =String()
    val PICK_IMAGE = 2
    private var saveState = false

    val db=Firebase.firestore
    val storage = Firebase.storage
    var storageRef = storage.reference
    private val vm: MyViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setother(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Access a Cloud Firestore instance from your Activity
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        val cameraButton = view.findViewById<ImageButton>(R.id.cameraButton)
        cameraButton.setOnClickListener {
            it.showContextMenu()
        }

        name.observe(context as AppCompatActivity, Observer { full_name.setText(it) })
        nkn.observe(context as AppCompatActivity, Observer { nickname.setText(it) })
        mail.observe(context as AppCompatActivity, Observer { email.setText(it) })
        tel.observe(context as AppCompatActivity, Observer { phone.setText(it) })
        loc.observe(context as AppCompatActivity, Observer { location.setText(it) })

        loc_button.setOnClickListener {
            showDialog()
        }

        if(savedInstanceState != null) {
            img.value = savedInstanceState.getString("img")
            name.value = SpannableStringBuilder(savedInstanceState.getString("name"))
            nkn.value = SpannableStringBuilder(savedInstanceState.getString("nick_n"))
            mail.value = SpannableStringBuilder(savedInstanceState.getString("mail"))
            tel.value = SpannableStringBuilder(savedInstanceState.getString("tel"))
            loc.value = SpannableStringBuilder(savedInstanceState.getString("loc"))
            coords = LatLng(
                savedInstanceState.getFloatArray("coords")?.get(0)?.toDouble()!!,
                savedInstanceState.getFloatArray("coords")?.get(1)?.toDouble()!!
            )
            fromCamera.value = savedInstanceState.getBoolean("fromCamera")
            fromGallery.value = savedInstanceState.getBoolean("fromGallery")
            if (img.value != "@drawable/ic_account")
                image.setImageURI(img.value?.toUri())
        }
        else {
            img.value = Users.loggedUser?.img
            if (img.value != "@drawable/ic_account")
                image.setImageURI(Uri.parse(img.value))

            if (Users.loggedUser?.name == "Full Name") name.value?.append("")
            else name.value = SpannableStringBuilder(Users.loggedUser?.name)
            if (Users.loggedUser?.nick == "Nickname") nkn.value?.append("")
            else nkn.value = SpannableStringBuilder(Users.loggedUser?.nick)
            if (Users.loggedUser?.mail == "email@address") mail.value?.append("")
            else mail.value = SpannableStringBuilder(Users.loggedUser?.mail)
            if (Users.loggedUser?.tel == "Phone Number") tel.value?.append("")
            else tel.value = SpannableStringBuilder(Users.loggedUser?.tel)
            if (Users.loggedUser?.loc == "Location") loc.value?.append("")
            else loc.value = SpannableStringBuilder(Users.loggedUser?.loc)
            if (Users.loggedUser?.coords == LatLng(0.0,0.0)) {vm.updateCurrentLoc(); coords=vm.currentLoc}
            else coords = Users.loggedUser?.coords!!

            fromCamera.value = arguments?.getBoolean("cam")
            fromGallery.value = arguments?.getBoolean("gall")
        }
        saveState = true
        //collega menu tendina con pulsante camera
        registerForContextMenu(cameraButton)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (saveState) {
            outState.putString("img", img.value.toString())
            outState.putString("name", full_name.text.toString())
            outState.putString("nick_n", nickname.text.toString())
            outState.putString("mail", email.text.toString())
            outState.putString("tel", phone.text.toString())
            outState.putString("loc", location.text.toString())
            outState.putBoolean("fromCamera", fromCamera.value!!)
            outState.putBoolean("fromGallery", fromGallery.value!!)
            outState.putString("currentPhotoUri", currentPhotoUri.value.toString())
            outState.putFloatArray("coords", floatArrayOf(coords.latitude.toFloat(), coords.longitude.toFloat()))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_editprofile, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_confirm -> {
            val b = Bundle()
            Log.d("Immagine", img.value!!)
            Users.loggedUser?.img = img.value!!
            Users.loggedUser?.name = full_name.text.toString()
            Users.loggedUser?.nick = nickname.text.toString()
            Users.loggedUser?.mail = email.text.toString()
            Users.loggedUser?.tel = phone.text.toString()
            Users.loggedUser?.loc = location.text.toString()
            Users.loggedUser?.coords = coords
            b.putBoolean("cam", fromCamera.value!!)
            b.putBoolean("gall", fromGallery.value!!)

            saveState = false
            savedatainfirestore()
            Users.immaginetemp = img.value!!
            Users.setUserImage(img.value!!)
            val navView: NavigationView = activity?.findViewById(R.id.nav_view)!!
            val headView = navView.getHeaderView(0)
            val nkn = headView.findViewById<TextView>(R.id.nkn_preview)
            if (nickname.text.toString() != nkn.text.toString())
                nkn.text = nickname.text
            findNavController().navigate(R.id.action_editProfileFragment_to_showProfileFragment, b)
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    public override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        (context as AppCompatActivity).menuInflater.inflate(R.menu.context_menu, menu)
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an file name
        val timeStamp: String = ""//SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        )
    }

    private val REQUEST_TAKE_PHOTO = 3

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity((context as AppCompatActivity).packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        (context as AppCompatActivity),
                        "com.example.seconhandmarketapp.fileprovider",
                        it
                    )
                    currentPhotoUri.value = photoURI;
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    private fun dispatchCropImageIntent(imageUri: Uri?) {
        val intent = CropImage.activity(imageUri)
            .setMinCropWindowSize(300,400)
            .setAspectRatio(3,4)
            .getIntent(requireContext());

        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }


    public override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.photo -> {
                dispatchTakePictureIntent()
                true
            }
            R.id.image_gallery -> {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_OPEN_DOCUMENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val imageView = (context as AppCompatActivity).findViewById<ImageView>(R.id.image)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == AppCompatActivity.RESULT_OK) {
            dispatchCropImageIntent(currentPhotoUri.value)

            fromCamera.value = true
            fromGallery.value = false
        }
        if (requestCode == PICK_IMAGE && data != null) {
            // success
            dispatchCropImageIntent(data.data!!)

            fromCamera.value = false
            fromGallery.value = true
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == AppCompatActivity.RESULT_OK) {
                val rotatedBitmap = handleSamplingAndRotationBitmap(
                    (context as AppCompatActivity),
                    result.getUri()
                )
                imageView?.setImageURI(result.getUri())
                img.value = result.getUri().toString()
            }
        }
    }

    @Throws(IOException::class)
    fun handleSamplingAndRotationBitmap(
        context: Context,
        selectedImage: Uri?
    ): Bitmap? {
        val MAX_HEIGHT = 1024
        val MAX_WIDTH = 1024
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var imageStream: InputStream? = selectedImage?.let {
            context.getContentResolver().openInputStream(
                it
            )
        }
        BitmapFactory.decodeStream(imageStream, null, options)
        imageStream?.close()
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        imageStream = selectedImage?.let { context.getContentResolver().openInputStream(it) }!!
        var img = BitmapFactory.decodeStream(imageStream, null, options)
        img = selectedImage?.let { img?.let { it1 -> rotateImageIfRequired(context, it1, it) } }
        return img
    }

    private fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int, reqHeight: Int
    ): Int { // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) { // Calculate ratios of height and width to requested height and width
            val heightRatio =
                Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio =
                Math.round(width.toFloat() / reqWidth.toFloat())
            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
// with both dimensions larger than or equal to the requested height and width.
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
            // This offers some additional logic in case the image has a strange
// aspect ratio. For example, a panorama may have a much larger
// width than height. In these cases the total pixels might still
// end up being too large to fit comfortably in memory, so we should
// be more aggressive with sample down the image (=larger inSampleSize).
            val totalPixels = width * height.toFloat()
            // Anything more than 2x the requested pixels we'll sample down further
            val totalReqPixelsCap = reqWidth * reqHeight * 2.toFloat()
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun rotateImageIfRequired(
        context: Context,
        img: Bitmap,
        selectedImage: Uri
    ): Bitmap? {
        val input: InputStream? = context.getContentResolver().openInputStream(selectedImage)
        val ei: ExifInterface
        if (Build.VERSION.SDK_INT > 23) ei = ExifInterface(input) else ei =
            ExifInterface(selectedImage.path)
        val orientation =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(img, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(img, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(img, 270)
            else -> img
        }
    }

    fun rotateImage(source: Bitmap, angle: Int): Bitmap? {
        val matrix: Matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        val rotatedImg: Bitmap = Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
        source.recycle()
        return rotatedImg
    }

    private fun savedatainfirestore(){
        val docData = hashMapOf(
            "full_name" to full_name.text.toString(),
            "nick_name" to nickname.text.toString(),
            "mail" to email.text.toString(),
            "location" to location.text.toString(),
            "phone_number" to phone.text.toString(),
            "coords" to coords.latitude.toString()+","+coords.longitude.toString()
        )

        if(getUser()!=null)
        db.collection("users").document(getUser()!!) //qui va messo l'effettivo uid
            .set(docData, SetOptions.merge())
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

        saveUserImage()
    }

    private fun saveUserImage() {

        if (img.value != "@drawable/ic_account") {
            var file: Uri = img.value!!.toUri()


            //Toast.makeText(context as AppCompatActivity, file.toString(), Toast.LENGTH_LONG).show()
            val usersRef = storageRef.child("user_images/" + getUser() + ".jpg")
            val uploadTask = usersRef.putFile(file)

// Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                // Handle unsuccessful uploads


            }.addOnSuccessListener { Log.d("ImageProfile", "file caricato") }

        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        Log.d("dialgo","positive")
        dialog as MapDialogFragment

        if(dialog.loc!= null && dialog.loc!= LatLng(0.0, 0.0)){
            Log.d("location", dialog.loc!!.toString())
            coords=dialog.loc
            val geocoder : Geocoder= Geocoder((context as AppCompatActivity), Locale.getDefault())
            val addresses : List<Address> = geocoder.getFromLocation(coords.latitude, coords.longitude, 1)
            if(addresses.isNotEmpty())
                location.text.apply {
                    if(addresses[0].thoroughfare!=null || addresses[0].locality!=null) {
                        clear()
                        append(
                            (if (addresses[0].thoroughfare != null) {
                                addresses[0].thoroughfare.toString() + (if (addresses[0].featureName != null) " " + addresses[0].featureName.toString() else "") + ", "
                            } else "")
                                    + if (addresses[0].locality != null) addresses[0].locality.toString()
                            else ""
                        )
                    }
                }
        }
    }

    private fun showDialog(){
        val maps : MapDialogFragment = MapDialogFragment.newInstance(coords, Action.EDIT)
        maps.setTargetFragment(this, 1)
        maps.show(parentFragmentManager, "MapDialogFragment")
    }
}



