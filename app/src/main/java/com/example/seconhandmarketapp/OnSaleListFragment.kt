package com.example.seconhandmarketapp

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_item_list.*
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.RuntimeException


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ItemListFragment.OnListFragmentInteractionListener] interface.
 */


class OnSaleListFragment : Fragment(), FiltersDialogFragment.NoticeDialogListener {
    private val db = Firebase.firestore
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyItemRecyclerViewAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var l = listOf<Item>()
    private var TAG = "users"
    private var first = false
    private val storage = Firebase.storage
    var storageRef = storage.reference
    private val vm: MyViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setOnSale()
    }

    override fun onResume() {
        super.onResume()
        vm.setOnSale()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR

        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        viewManager = LinearLayoutManager(context)
        viewAdapter = MyItemRecyclerViewAdapter(l, null, listener, null, null, true, false, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.list).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        if (OnSaleList.isEmpty())
            realtimeUpdateDB()
        else {
            viewAdapter.setItems(OnSaleList.getItemList())
            view.noItemLabel.text = ""
        }

        var fab: FloatingActionButton = view.fab_list
        fab.visibility = View.GONE
        fab = view.fab_refresh
        fab.visibility = View.VISIBLE
        fab.setOnClickListener {
            AlertDialog.Builder(context as AppCompatActivity)
                .setTitle("Do you want to refresh items?")
                .setPositiveButton("Ok") { dialog: DialogInterface, which: Int ->
                    realtimeUpdateDB()
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { dialog: DialogInterface, which: Int -> dialog.cancel() }
                .create()
                .show()
        }

        view.swipe_layout.setOnRefreshListener {
            realtimeUpdateDB()
            view.swipe_layout.isRefreshing=false
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun realtimeUpdateDB() {
        val query = db.collectionGroup("items")
        query
            .addSnapshotListener { items, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed", e)
                    return@addSnapshotListener
                }
                OnSaleList.clear()
                for (item in items!!) {
                    Log.d(TAG, "${item.id} => ${item.data}")
                    if (item.data["userID"] != Users.getUser() && item.data["isSold"] != true && item.data["isHide"] != true) {
                        val i = Item(item.data["itemID"].toString(),
                            item.data["userID"].toString(),
                            "@drawable/itemicon",
                            //downloadPhoto(item.data["userID"].toString(),item.data["itemID"].toString()),
                            item.data["title"].toString(),
                            item.data["description"].toString(),
                            item.data["price"].toString(),
                            item.data["location"].toString(),
                            item.data["expirydate"].toString(),
                            item.data["category"].toString(),
                            item.data["isSold"] as Boolean,
                            item.data["isHide"] as Boolean,
                            item.data["interestedUsers"] as ArrayList<String>)
                        OnSaleList.addItem(i)

                    }

                }
                viewAdapter.onSearchList = false
                viewAdapter.setItems(OnSaleList.getItemList())
                downloadPhoto()
                if(!first) {
                    if (!OnSaleList.isEmpty())
                        noItemLabel.text = ""
                    else
                        noItemLabel.text = getString(R.string.noitem)
                    first = true
                }
                Log.d(TAG, "Listen succeed")
            }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.options_menu, menu)

        val searchView =
            SearchView((context as MainActivity).supportActionBar?.themedContext ?: context)
        menu.findItem(R.id.search).apply {
            setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItem.SHOW_AS_ACTION_IF_ROOM)
            actionView = searchView
            setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                    searchView.requestFocus()
                    //apro la tastiera
                    val input = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    input.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                    viewAdapter.setItems(OnSaleList.getItemList())
                    viewAdapter.onSearchList = false
                    return true
                }
            })
        }

        searchView.isIconifiedByDefault = false
        searchView.queryHint = "Search"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsMenuClosed(menu: Menu) {
        viewAdapter.setItems(OnSaleList.getItemList())
        super.onOptionsMenuClosed(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.search -> {
            true
        }
        R.id.filters -> {
            showDialog()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun showDialog(){
        val filters : FiltersDialogFragment = FiltersDialogFragment()
        filters.setTargetFragment(this, 1)
        filters.show(parentFragmentManager, "FiltersDialogFragment")
    }


    private fun downloadPhoto()
    {
        val localFileList: MutableList<File> = arrayListOf()
        val path: MutableList<String> = arrayListOf()
        for ((i, item) in OnSaleList.list.withIndex()) {
            MainScope().launch {
                path.add("@drawable/itemicon")
                localFileList.add(File.createTempFile("image" + i, "jpg"))
                withContext(Dispatchers.IO) {
                    storageRef.child("user_images/" + item.userID + "/" + item.id + ".jpg")
                        .getFile(localFileList[i])
                        .addOnSuccessListener {
                            path[i] = localFileList[i].absolutePath
                            Log.d("foto", path[i])
                            OnSaleList.modifyItem(Item(item.id.toString(), item.userID, path[i], item.titolo!!, item.desc!!, item.prezzo!!, item.loc!!, item.exp!!,
                                item.categoria!!, item.isSold, item.isHide, item.interestedUsers!!))
                            viewAdapter.setItems(OnSaleList.getItemList())
                        }.addOnFailureListener {
                            path[i] = localFileList[i].absolutePath
                            Log.d("foto", path[i])
                            OnSaleList.modifyItem(Item(item.id.toString(), item.userID, "@drawable/itemicon", item.titolo!!, item.desc!!, item.prezzo!!, item.loc!!, item.exp!!,
                                item.categoria!!, item.isSold, item.isHide, item.interestedUsers!!))
                            viewAdapter.setItems(OnSaleList.getItemList())
                        }
                }
            }
        }
    }


    fun setOnListFragmentInteractionListener(callback: OnListFragmentInteractionListener) {
        this.listener = callback
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Item?)
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
            OnSaleListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        Log.d("dialgo","positive")
        dialog as FiltersDialogFragment
        if (dialog.fromPrice != "" && dialog.toPrice != "") {
            viewAdapter.filter.filter(":price\\" + dialog.fromPrice + "to" + dialog.toPrice)
        }
        if(dialog.categoria != null && dialog.categoria != "") {
            Log.d("filter", "${dialog.categoria}")
            viewAdapter.filter.filter(":category\\" + dialog.categoria)
        }
        if(!dialog.fromExpiry.value.isNullOrEmpty() && !dialog.toExpiry.value.isNullOrEmpty()) {
            Log.d("filter", dialog.fromExpiry.value.toString())
            viewAdapter.filter.filter(":expirydate\\" + dialog.fromExpiry.value + "to" + dialog.toExpiry.value)
        }
        if(dialog.location!= null && dialog.location!= ""){
            Log.d("filter", dialog.location!!)
            viewAdapter.filter.filter(":location\\" + dialog.location)
        }
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {
        viewAdapter.onSearchList = false
        viewAdapter.setItems(OnSaleList.getItemList())
    }


}