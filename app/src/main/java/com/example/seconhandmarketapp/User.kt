package com.example.seconhandmarketapp

import android.content.Context
import android.content.SharedPreferences
import android.provider.Settings.Global.getString
import com.google.android.gms.maps.model.LatLng

object Users {
    var loggedUser: User? = null
    var viewedUser: User? = null
    var immaginetemp:String=""
    var registration=false
    var googleimage:String=""
    fun getUser(): String? {
        return loggedUser?.uid
    }
    fun getUserData() :User?
    {if(loggedUser!=null)
        return loggedUser!!
        else
        return null
    }
    fun logUser(uid: String){
        loggedUser=User(uid)
    }
    fun riempiUser(name: String="Full Name", nick: String="Nickname",mail: String="email@address",tel: String="Phone Number", loc: String="Location", img: String="@drawable/ic_account", coords: LatLng= LatLng(0.0,0.0)){
        if(loggedUser!=null) {
            loggedUser!!.name =name
            loggedUser!!.nick=nick
            loggedUser!!.mail=mail
            loggedUser!!.tel=tel
            loggedUser!!.loc=loc
            loggedUser!!.img=img
            loggedUser!!.coords=coords
        }
    }

    fun setUserImage(uri:String )
    {
        if(loggedUser!=null)
            loggedUser!!.img=uri
    }

}


data class User(var uid: String, var name: String="Full Name", var nick: String="Nickname", var mail: String="email@address", var tel: String="Phone Number", var loc: String="Location", var img: String="@drawable/ic_account", var coords: LatLng= LatLng(0.0,0.0))
