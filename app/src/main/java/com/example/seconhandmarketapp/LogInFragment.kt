package com.example.seconhandmarketapp

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.log_in.*


class LogInFragment : Fragment() {

    lateinit var password: EditText
    lateinit var email: EditText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR;

        return inflater.inflate(R.layout.log_in, container, false)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        email=username
        password=pws
        loadingPanel.setVisibility(View.GONE);

      glogin.setOnClickListener{
            val activity: Activity? = activity
            if (activity is Auth) {
                val myactivity: Auth? = activity as Auth?
                myactivity!!.googleSignIn()
            }
        }

        sign_in.setOnClickListener{
            if(validateEmail() && validatePassword()){
            val activity: Activity? = activity
            if (activity is Auth) {
                val myactivity: Auth? = activity as Auth?
                myactivity!!.signIn(email.text.toString(),password.text.toString())
            }}

        }

        sign_up.setOnClickListener{
            findNavController().navigate(R.id.action_logInFragment_to_signUpFragment)
        }

        send_email.setOnClickListener()
        {
            if(validateEmail()){
                val activity: Activity? = activity
                if (activity is Auth) {
                    val myactivity: Auth? = activity as Auth?
                    myactivity!!.resetPassword(email.text.toString())
                }}
        }

        super.onCreate(savedInstanceState)
    }



    fun validateEmail(): Boolean{
        if(email.text.isEmpty())
        {
            email.setError(getString(R.string.mailempty))
            return false
        }
        else
        {
            if(email.text.contains("@") && email.text.contains("."))
            {
                return true
            }
            else
            {
                email.setError(getString(R.string.mailerror))
                return false
            }
        }
    }
    fun validatePassword(): Boolean{
        if(password.text.isEmpty())
        {
            password.setError(getString(R.string.pwsempty))
            return false
        }
        else
        {
            if(password.text.length>5)
            {
                return true
            }
            else
            {
                password.setError(getString(R.string.pwserror))
                return false
            }
        }
    }
}
