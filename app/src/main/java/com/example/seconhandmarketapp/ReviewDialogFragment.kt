package com.example.seconhandmarketapp

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import kotlinx.android.synthetic.main.fragment_item_list.*
import kotlinx.android.synthetic.main.review_dialog.*
import java.lang.ClassCastException

class ReviewDialogFragment : DialogFragment() {
    private lateinit var listener: NoticeDialogListener
    private val vm: MyViewModel by activityViewModels()
    var txt : String? = null
    var rating : Float = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setother(true)
    }

    interface NoticeDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment)
        fun onDialogNegativeClick(dialog: DialogFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.review_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_ok.setOnClickListener {
            Log.d("rating","${ratingBar.rating}")
            if(ratingBar.rating != 0f){
                txt = edit_comment.text.toString()
                rating = ratingBar.rating
                listener.onDialogPositiveClick(this)
                this.dismiss()
            }
            else{
                Toast.makeText(context,"Select a rating",Toast.LENGTH_LONG).show()
//                var alert : AlertDialog? = activity?.let {
//                    val builder = AlertDialog.Builder(it)
//                    builder.apply {
//                        set
//                    }
//                    builder.create()
//                }
            }

        }
        button_cancel.setOnClickListener {
            this.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ConstraintLayout.LayoutParams.MATCH_PARENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = targetFragment as NoticeDialogListener
        } catch (e: ClassCastException) {
            throw java.lang.ClassCastException((context.toString() + "must implement NoticeDialogListener"))
        }
    }
}