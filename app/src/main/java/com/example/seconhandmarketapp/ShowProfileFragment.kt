package com.example.seconhandmarketapp

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.location.Location
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.core.net.toUri
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import java.io.File
import kotlinx.android.synthetic.main.fragment_show_profile.*
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.seconhandmarketapp.Users.googleimage
import com.example.seconhandmarketapp.Users.immaginetemp
import com.example.seconhandmarketapp.Users.loggedUser
import com.example.seconhandmarketapp.Users.registration
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.fragment_show_profile.view.*
import kotlinx.android.synthetic.main.item_edit_fragment.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.io.InputStream

class ShowProfileFragment : Fragment(){
    private val img = MutableLiveData<String>()
    private val name = MutableLiveData<String>()
    private val nkn = MutableLiveData<String>()
    private val mail = MutableLiveData<String>()
    private val tel = MutableLiveData<String>()
    private val loc = MutableLiveData<String>()
    private val fromCamera = MutableLiveData<Boolean>()
    private val fromGallery = MutableLiveData<Boolean>()
    private var rating = MutableLiveData<Float>()
    private var saveState = false
    private val db = Firebase.firestore
    private val storage = Firebase.storage
    private var storageRef = storage.reference
    private val vm: MyViewModel by activityViewModels()
    private var isViewed : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setProfile()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_show_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        log_out.setOnClickListener {
            val mAuth = FirebaseAuth.getInstance();
            if(mAuth!= null)
            {
                mAuth.signOut()
                val intent = Intent(context as AppCompatActivity, Auth::class.java).apply {
                }
                Users.loggedUser=null
                Users.immaginetemp=""
                ItemList.clear()
                OnSaleList.clear()
                ItemOfInterestList.clear()
                BoughtList.clear()
                startActivity(intent)
            }
        }

        name.observe(context as AppCompatActivity, Observer { full_name.text = it })
        nkn.observe(context as AppCompatActivity, Observer { nickname.text = it })
        mail.observe(context as AppCompatActivity, Observer { email.text = it })
        tel.observe(context as AppCompatActivity, Observer { phone.text = it })
        loc.observe(context as AppCompatActivity, Observer { location.text = it })
        rating.observe(context as AppCompatActivity, Observer { ratingBar.rating = it })
        rating.observe(context as AppCompatActivity, Observer { ratingNum.text = it.toString() })

        loc_button.setOnClickListener {
            showDialog()
        }

        //vanno inizializzati con le shared preferencies
        fromCamera.value = false
        fromGallery.value = false
        saveState = true

        if(savedInstanceState != null) {
            isViewed=savedInstanceState.getBoolean("isV")
            img.value = savedInstanceState.getString("img")
//            if(isViewed) {
            name.value = savedInstanceState.getString("name")
            nkn.value = savedInstanceState.getString("nick_n")
            mail.value = savedInstanceState.getString("mail")
            tel.value = savedInstanceState.getString("tel")
            loc.value = savedInstanceState.getString("loc")
//            }
            fromCamera.value=savedInstanceState.getBoolean("fromCamera")
            fromGallery.value=savedInstanceState.getBoolean("fromGallery")
            rating.value=savedInstanceState.getFloat("rating")
            isViewed=savedInstanceState.getBoolean("isV")
            if(img.value != "@drawable/ic_account")
                Glide.with(context as AppCompatActivity)
                    .load(img.value)
                    .into(image)
        }
        else if (arguments!=null && !arguments?.getString("userID").isNullOrEmpty()) {
            name.value = Users.viewedUser?.nick
            nkn.value = Users.viewedUser?.mail
            mail.value = Users.viewedUser?.tel
            tel.value = Users.viewedUser?.loc
            img.value = "@drawable/ic_account"
            getRating(Users.viewedUser?.uid!!)
            MainScope().launch {
                var path = "@drawable/itemicon"
                val localFileList = File.createTempFile("ViewedUserImage", "jpg")
                withContext(Dispatchers.IO) {
                    storageRef.child("user_images/" + Users.viewedUser?.uid + ".jpg")
                        .getFile(localFileList)
                        .addOnSuccessListener {
                            path = localFileList.absolutePath
                            Log.d("foto", path)
                            Users.viewedUser?.img = path
                            img.value = Users.viewedUser?.img
                            image.setImageURI(img.value?.toUri())
                        }
                }
            }
            isViewed=true
        }
        else if (Users.loggedUser != null) {
            name.value = Users.loggedUser?.name
            nkn.value = Users.loggedUser?.nick
            mail.value = Users.loggedUser?.mail
            tel.value = Users.loggedUser?.tel
            loc.value = Users.loggedUser?.loc
            getRating(Users.loggedUser?.uid!!)

            Users.loggedUser?.img = Users.immaginetemp

            img.value = Users.loggedUser?.img

            if(registration){
                Glide.with(context as AppCompatActivity)
                    .load(googleimage)
                    .into(image)
                img.value = googleimage
            }
            else {
                if (img.value != "@drawable/ic_account") {
                    val imgPreview =
                        (context as AppCompatActivity).findViewById<NavigationView>(R.id.nav_view)
                            .findViewById<ImageView>(R.id.image_preview)
                    image.setImageURI(img.value?.toUri())
                    imgPreview.setImageURI(img.value?.toUri())
                }
            }



//            saveData()
        }

        if(isViewed)
        {
            location.isVisible=false
            phone.isVisible=false
            loc_button.isVisible=false
            log_out.isVisible=false
            setHasOptionsMenu(false)
        }
    }



    private fun getRating(uid: String){
        var ratingTmp: Double = 0.0
        var i : Int = 0
        db.collection("users").document(uid).collection("items").get()
            .addOnSuccessListener { items ->
                for(item in items){
                    if(item.contains("rating")) {
                        ratingTmp += item.getDouble("rating")!!
                        i++
                    }
                }
                if(i!=0)
                    ratingTmp /= i
                rating.value = ratingTmp.toFloat()
            }
            .addOnFailureListener {
                rating.value = 0f
            }
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (saveState == true) {
            outState.putString("img", img.value!!)
            outState.putString("name", name.value!!)
            outState.putString("nick_n", nkn.value!!)
            outState.putString("mail", mail.value!!)
            outState.putString("tel", if(isViewed) mail.value else tel.value!!)
            outState.putString("loc", if(isViewed) mail.value else loc.value!!)
            outState.putBoolean("fromCamera", fromCamera.value!!)
            outState.putBoolean("fromGallery", fromGallery.value!!)
            outState.putBoolean("isV", isViewed)
            outState.putFloat("rating",rating.value!!)
        }
    }

    ///mi linka il menu da menu/..
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_showprofile, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_edit -> {
            val b = editProfile()

            saveState = false
            findNavController().navigate(R.id.action_showProfileFragment_to_editProfileFragment, b)
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    private fun editProfile(): Bundle {
        val bundle = Bundle()

        bundle.putString("img", img.value)
        bundle.putString("name", name.value)
        bundle.putString("nkn", nkn.value)
        bundle.putString("mail", mail.value)
        bundle.putString("tel", tel.value)
        bundle.putString("loc", loc.value)
        bundle.putBoolean("cam", fromCamera.value!!)
        bundle.putBoolean("gall", fromGallery.value!!)
        bundle.putFloatArray("coords", floatArrayOf(Users.loggedUser?.coords!!.latitude.toFloat(), Users.loggedUser?.coords!!.longitude.toFloat()))
        return bundle
    }

    private fun saveData() {
        // salvo l'immagine nuova sul filesystem e salvo l'URI dentro img
        if (img.value != "@drawable/ic_account") {
            val filename = getString(R.string.profile_pic_key)
            val stream =
                (context as AppCompatActivity).openFileOutput(filename, Context.MODE_PRIVATE)
            val bitmap = image.drawable.toBitmap()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
            img.value = File((context as AppCompatActivity).filesDir, filename).toUri().toString()
        }
    }

    @Throws(IOException::class)
    fun handleSamplingAndRotationBitmap(
        context: Context,
        selectedImage: Uri?
    ): Bitmap? {
        val MAX_HEIGHT = 1024
        val MAX_WIDTH = 1024
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var imageStream: InputStream? = selectedImage?.let {
            context.getContentResolver().openInputStream(
                it
            )
        }
        BitmapFactory.decodeStream(imageStream, null, options)
        imageStream?.close()
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        imageStream = selectedImage?.let { context.getContentResolver().openInputStream(it) }!!
        var img = BitmapFactory.decodeStream(imageStream, null, options)
        img = selectedImage?.let { img?.let { it1 -> rotateImageIfRequired(context, it1, it) } }
        return img
    }

    private fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int, reqHeight: Int
    ): Int { // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) { // Calculate ratios of height and width to requested height and width
            val heightRatio =
                Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio =
                Math.round(width.toFloat() / reqWidth.toFloat())
            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
// with both dimensions larger than or equal to the requested height and width.
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
            // This offers some additional logic in case the image has a strange
// aspect ratio. For example, a panorama may have a much larger
// width than height. In these cases the total pixels might still
// end up being too large to fit comfortably in memory, so we should
// be more aggressive with sample down the image (=larger inSampleSize).
            val totalPixels = width * height.toFloat()
            // Anything more than 2x the requested pixels we'll sample down further
            val totalReqPixelsCap = reqWidth * reqHeight * 2.toFloat()
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun rotateImageIfRequired(
        context: Context,
        img: Bitmap,
        selectedImage: Uri
    ): Bitmap? {
        val input: InputStream? = context.getContentResolver().openInputStream(selectedImage)
        val ei: ExifInterface
        if (Build.VERSION.SDK_INT > 23) ei = ExifInterface(input) else ei =
            ExifInterface(selectedImage.path)
        val orientation =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(img, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(img, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(img, 270)
            else -> img
        }
    }

    private fun rotateImage(source: Bitmap, angle: Int): Bitmap? {
        val matrix: Matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        val rotatedImg: Bitmap = Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
        source.recycle()
        return rotatedImg
    }

    private fun showDialog(){
        val location = if((isViewed && Users.viewedUser?.coords==LatLng(0.0,0.0)) || (!isViewed && Users.loggedUser?.coords==LatLng(0.0,0.0))) vm.currentLoc
                        else if(isViewed) Users.viewedUser?.coords
                        else Users.loggedUser?.coords
        val maps : MapDialogFragment = MapDialogFragment.newInstance(if(location==null) vm.currentLoc else location, Action.SHOW)
        maps.setTargetFragment(this, 1)
        maps.show(parentFragmentManager, "MapDialogFragment")
    }
}