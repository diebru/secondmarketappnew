package com.example.seconhandmarketapp

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.location.Address
import android.location.Geocoder
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment.DIRECTORY_PICTURES
import android.provider.MediaStore
import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.seconhandmarketapp.Users.getUser
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.item_edit_fragment.*
import kotlinx.android.synthetic.main.item_edit_fragment.category
import kotlinx.android.synthetic.main.item_edit_fragment.description
import kotlinx.android.synthetic.main.item_edit_fragment.expiry_date
import kotlinx.android.synthetic.main.item_edit_fragment.image
import kotlinx.android.synthetic.main.item_edit_fragment.location
import kotlinx.android.synthetic.main.item_edit_fragment.price
import kotlinx.android.synthetic.main.item_edit_fragment.title
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.util.*
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.sign_up.*

class ItemEditFragment :Fragment(), MapDialogFragment.MapDialogListener {

    private var img = MutableLiveData<String>()
    private var currentPhotoUri = MutableLiveData<Uri>()
    private var titolo = MutableLiveData<Editable>()
    private var desc = MutableLiveData<Editable>()
    private var categoria = MutableLiveData<String>()
    private var prezzo = MutableLiveData<Editable>()
    private var exp = MutableLiveData<String>()
    private var loc = MutableLiveData<Editable>()
    lateinit var coords : LatLng
    var coordsNotSet = false
    private var fromCamera = MutableLiveData<Boolean>()
    private var fromGallery = MutableLiveData<Boolean>()
    private val PICK_IMAGE = 2
    private var saveState = false
    private var boolSold = false
    private var boolHide = false
    private val db = Firebase.firestore
    private var itemId= -1
    private var sellID = ""
    private var userInt = arrayListOf("")
    val storage = Firebase.storage
    var storageRef = storage.reference
    private var changeSoldState = false
    private var changeHideState = false
    private var buyer = ""
    private val vm: MyViewModel by activityViewModels()
    private var newImage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setother(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.item_edit_fragment, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        imageButton.setOnClickListener { it.showContextMenu() }

        initialize()

        if (arguments?.getString("img") != null) {
            img.value = arguments?.getString("img")
            if (img.value != "@drawable/itemicon")    image.setImageURI(img.value?.toUri())
        }

        if(savedInstanceState != null) {
            img.value = savedInstanceState.getString("img")
            titolo.value = SpannableStringBuilder(savedInstanceState.getString("title"))
            desc.value = SpannableStringBuilder(savedInstanceState.getString("desc"))
            prezzo.value = SpannableStringBuilder(savedInstanceState.getString("price"))
            categoria.value = savedInstanceState.getString("cat")
            exp.value = savedInstanceState.getString("exp")
            loc.value = SpannableStringBuilder(savedInstanceState.getString("loc"))
            fromCamera.value = savedInstanceState.getBoolean("fromCamera")
            fromGallery.value = savedInstanceState.getBoolean("fromGallery")
            boolHide = savedInstanceState.getBoolean("h")
            boolSold = savedInstanceState.getBoolean("s")
            coords = LatLng(
                savedInstanceState.getFloatArray("coords")?.get(0)?.toDouble()!!,
                savedInstanceState.getFloatArray("coords")?.get(1)?.toDouble()!!
            )
            coordsNotSet = savedInstanceState.getBoolean("cNS")
        }

        registerForContextMenu(imageButton)
        datePicker()        //DATE PICKER DIALOG
        spinner()           //SPINNER
        soldCheck()         //SWITCH SOLD
        hideCheck()         //SWITCH HIDE
    }

    private fun initialize() {
        titolo.observe(context as AppCompatActivity, Observer { title.text = it })
        desc.observe(context as AppCompatActivity, Observer { description.text = it })
        prezzo.observe(context as AppCompatActivity, Observer { price.text = it })
        categoria.observe(context as AppCompatActivity, Observer { category.text = it })
        loc.observe(context as AppCompatActivity, Observer { location.text = it })
        exp.observe(context as AppCompatActivity, Observer { expiry_date.text = it })

        img.value = "@drawable/itemicon"
        titolo.value = SpannableStringBuilder("")
        desc.value = SpannableStringBuilder("")
        prezzo.value = SpannableStringBuilder("")
        categoria.value = getString(R.string.cat)
        loc.value = SpannableStringBuilder("")
        exp.value = getString(R.string.exp)
        coords = vm.currentLoc
        coordsNotSet=true
        fromCamera.value = false
        fromGallery.value = false
        saveState = true

        itemId = ItemList.getSize() + 1

        loc_button.setOnClickListener {
            showDialog()
        }

        if(arguments?.getString("userID") != null)
            sellID = arguments?.getString("userID")!!
        if(arguments?.getString("itemID") != null) {
            itemId = arguments?.getString("itemID")!!.toInt()
            db.collection("users").document(sellID).collection("items").document(itemId.toString()).get()
                .addOnSuccessListener {res ->
                    val d = res.data
                    titolo.value = SpannableStringBuilder(d?.get("title").toString())
                    sellID = d?.get("userID").toString()
                    desc.value = SpannableStringBuilder(d?.get("description").toString())
                    exp.value = d?.get("expirydate").toString()
                    prezzo.value = SpannableStringBuilder(d?.get("price").toString())
                    loc.value = SpannableStringBuilder(d?.get("location").toString())
                    categoria.value = d?.get("category").toString()
                    boolHide = d?.get("isHide") as Boolean
                    boolSold = d["isSold"] as Boolean
                    userInt = d["interestedUsers"] as ArrayList<String>
                    buyer = d["buyer"].toString()
                    coords = if(d["coords"]!=null) {
                        coordsNotSet=false
                        val l = d["coords"].toString().split(",")
                        LatLng(l[0].toDouble(), l[1].toDouble())
                    }
                    else vm.currentLoc
                    if (boolHide)   switch_hide.isChecked = true
                    if (boolSold)   switch_sold.isChecked = true
                    changeSoldState = boolSold
                    changeHideState = boolHide
                }
                .addOnFailureListener { Toast.makeText(context as AppCompatActivity,"Error to load data", Toast.LENGTH_LONG).show() }
        }
    }

    private fun soldCheck() {
        switch_sold.setOnCheckedChangeListener{ buttonView, isChecked ->
            if(isChecked) {
                if(buyer == "") {
                    switch_sold.isChecked = false
                    val lista: MutableList<CharSequence> = mutableListOf()
                    for (member in userInt)
                        lista.add("")
                    for ((i, member) in userInt.withIndex()) {
                        if (member != "") {
                            db.collection("users").document(member).get()
                                .addOnSuccessListener { res ->
                                    val d = res.data
                                    if (d != null)  lista[i] = d.getValue("nick_name").toString()
                                    if (lista[lista.size - 1] != "" && lista.size > 0)
                                        AlertDialog.Builder(context as AppCompatActivity)
                                            .setTitle(R.string.selectbuyer)
                                            .setItems(lista.toTypedArray()) { dialog, which ->
                                                buyer = userInt[which]
                                                sold.text = getString(R.string.soldout)
                                                boolSold = true
                                                switch_sold.isChecked = true
                                            }
                                            .setNegativeButton("Back") { dialog: DialogInterface, which: Int -> dialog.cancel() }
                                            .create()
                                            .show()
                                }
                                .addOnFailureListener {
                                    Toast.makeText(
                                        context as AppCompatActivity,
                                        "Error to load users",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                        } else {
                            AlertDialog.Builder(context as AppCompatActivity)
                                .setTitle(R.string.zerousers)
                                .setNegativeButton("Back") { dialog: DialogInterface, which: Int -> dialog.cancel() }
                                .create()
                                .show()
                        }
                    }
                }
            }
            else {
                buyer = ""
                sold.text = getString(R.string.available)
                boolSold = false
            }
        }
    }

    private fun createNotificationList() {
        if(changeSoldState != boolSold) {
            for(member in userInt) {
                if(boolSold && member != "") {
                    if (member == buyer)    sendNotification("Sold", member, itemId.toString(), Users.getUserData()?.nick.toString(), titolo.value.toString(), "buyer", "notificationSold")
                    else                    sendNotification("Sold", member, itemId.toString(), Users.getUserData()?.nick.toString(), titolo.value.toString(), "sold", "notificationSold")
                }
                else if(!boolSold && member != "")  sendNotification("Available", member, itemId.toString(), Users.getUserData()?.nick.toString(), titolo.value.toString(), "available","notificationSold")
            }
        }
        if(changeHideState != boolHide) {
            for(member in userInt) {
                if(boolHide && member != "")        sendNotification("Hide", member, itemId.toString(), Users.getUserData()?.nick.toString(), titolo.value.toString(), "hide","notificationHide")
                else if(!boolHide && member != "")  sendNotification("Visible", member, itemId.toString(), Users.getUserData()?.nick.toString(), titolo.value.toString(), "visible","notificationHide")
            }
        }
    }

    private fun sendNotification(docID: String, recipientID: String, itemID: String, nickn: String, itemName: String, message: String, collectionName: String) {
        val docData = hashMapOf(
            "recipientID" to recipientID,
            "itemID" to itemID,
            "nick_name" to nickn,
            "item_name" to itemName,
            "message" to message
        )

        db.collection("users").document(recipientID)
            .collection("items").document(itemID)
            .collection(collectionName).document(docID)
            .set(docData)
            .addOnSuccessListener { Log.d("Documento notifica", "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w("Documento notifica", "Error writing document", e) }
            .addOnCompleteListener { db.collection("users").document(recipientID)
                .collection("items").document(itemID)
                .collection(collectionName).document(docID).delete() }
    }

    private fun hideCheck() {
        switch_hide.setOnCheckedChangeListener{ buttonView, isChecked ->
            if(isChecked) {
                hide.setText(R.string.hidden)
                boolHide = true
            }
            else {
                hide.setText(R.string.visible)
                boolHide = false
            }
        }
    }

    private fun datePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)+1
        val day = c.get(Calendar.DAY_OF_MONTH)

        dateButton.setOnClickListener {
            val dpd = DatePickerDialog (context as AppCompatActivity, DatePickerDialog.OnDateSetListener{
                    view, mYear, mMonth, mDay -> exp.value =(""+ mDay +"/"+ mMonth +"/"+ mYear)
            }, year, month, day)

            dpd.show()
        }
    }

    private fun spinner() {
        spin.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View?, position: Int, id: Long){
                // Display the selected item text on text view
                if (position != 0)
                    categoria.value = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>){
                //Nothing
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if(saveState) {
            outState.putString("img", img.value!!)
            outState.putString("title", title.text.toString())
            outState.putString("desc", description.text.toString())
            outState.putString("price", price.text.toString())
            outState.putString("cat", category.text.toString())
            outState.putString("loc", location.text.toString())
            outState.putString("exp", expiry_date.text.toString())
            outState.putBoolean("fromCamera", fromCamera.value!!)
            outState.putBoolean("fromGallery", fromGallery.value!!)
            outState.putString("currentPhotoUri", currentPhotoUri.value.toString());
            outState.putFloatArray("coords", floatArrayOf(coords.latitude.toFloat(), coords.longitude.toFloat()))
            outState.putBoolean("cNS", coordsNotSet)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_item_menu, menu)
        super.onCreateOptionsMenu(menu,inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_edit -> {

            saveState = false
            controlData()
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    private fun controlData() {
        val b = Bundle()
        if(title.text.toString() == ""
            || price.text.toString() == ""
            || location.text.toString() == ""
            || category.text.toString() == "Category"
            || expiry_date.text.toString() == "Select Expiry Date"
            || coords == LatLng(0.0,0.0)
            || coordsNotSet
        ) {
            AlertDialog.Builder(context as AppCompatActivity)
                .setTitle(R.string.missingdata)
                .setMessage(R.string.datatoenter)
                .setPositiveButton("Ok") {
                        dialog: DialogInterface, which: Int -> Toast.makeText(context as AppCompatActivity,"Fill in the fields",Toast.LENGTH_LONG).show()
                    dialog.cancel()
                }
                .create()
                .show()
        }
        else {
            var new = true
            val item = ItemList.getItem(itemId)
            if (item != null) {
                new = false
                ItemList.modifyItem(Item(itemId.toString(), getUser()!!, img.value!!, title.text.toString(), description.text.toString(), price.text.toString(), location.text.toString(),
                    expiry_date.text.toString(), category.text.toString(), boolSold, boolHide, userInt, coords))
            }
            saveFirebaseData()
            if(newImage)    saveUserImage()
            createNotificationList()
            b.putBoolean("new", new)
            findNavController().navigate(R.id.action_itemEditFragment_to_itemListFragment, b)
            ItemList.notEmpty = true
        }
    }

    private fun saveFirebaseData(){
        val docData = hashMapOf(
            "itemID" to itemId.toString(),
            "userID" to getUser()!!,
            "title" to title.text.toString(),
            "description" to description.text.toString(),
            "price" to price.text.toString(),
            "location" to location.text.toString(),
            "expirydate" to expiry_date.text.toString(),
            "category" to category.text.toString(),
            "isSold" to boolSold,
            "isHide" to boolHide,
            "interestedUsers" to userInt,
            "buyer" to buyer,
            "coords" to coords.latitude.toString()+","+coords.longitude.toString()
        )

        if(getUser() !=null)
            db.collection("users").document(getUser()!!).collection("items").document(itemId.toString()) //qui va messo l'effettivo uid
                .set(docData, SetOptions.merge())
                .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        (context as AppCompatActivity).menuInflater.inflate(R.menu.context_menu, menu)
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = ""//SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = context?.getExternalFilesDir(DIRECTORY_PICTURES)
        newImage = true
        return File.createTempFile(
            "JPEG_${timeStamp}_", // prefix
            ".jpg", // suffix
            storageDir // directory
        )
    }

    private val REQUEST_TAKE_PHOTO = 3

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity((context as AppCompatActivity).packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        context as AppCompatActivity,
                        "com.example.seconhandmarketapp.fileprovider",
                        it
                    )
                    currentPhotoUri.value = photoURI;
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    private fun dispatchCropImageIntent(imageUri: Uri?) {
        newImage = true
        val intent = CropImage.activity(imageUri)
            .setMinCropWindowSize(400,300)
            .setAspectRatio(4,3)
            .getIntent(requireContext());

        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.photo -> {
                dispatchTakePictureIntent()
                true
            }
            R.id.image_gallery -> {

                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_OPEN_DOCUMENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val imageView = (context as AppCompatActivity).findViewById<ImageView>(R.id.image)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == AppCompatActivity.RESULT_OK) {
            dispatchCropImageIntent(currentPhotoUri.value)

            fromCamera.value = true
            fromGallery.value = false
        }
        if (requestCode == PICK_IMAGE && data != null) {
            // success
            dispatchCropImageIntent(data.data!!)

            fromCamera.value = false
            fromGallery.value = true
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == AppCompatActivity.RESULT_OK) {
                val rotatedBitmap = handleSamplingAndRotationBitmap(
                    (context as AppCompatActivity),
                    result.getUri()
                )
                imageView?.setImageURI(result.getUri())
                img.value = result.getUri().toString()
            }
        }
    }

    @Throws(IOException::class)
    fun handleSamplingAndRotationBitmap(
        context: Context,
        selectedImage: Uri?
    ): Bitmap? {
        val MAX_HEIGHT = 1024
        val MAX_WIDTH = 1024
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var imageStream: InputStream? = selectedImage?.let {
            context.getContentResolver().openInputStream(
                it
            )
        }
        BitmapFactory.decodeStream(imageStream, null, options)
        imageStream?.close()
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        imageStream = selectedImage?.let { context.getContentResolver().openInputStream(it) }!!
        var img = BitmapFactory.decodeStream(imageStream, null, options)
        img = selectedImage.let { img?.let { it1 -> rotateImageIfRequired(context, it1, it) } }
        return img
    }

    private fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int, reqHeight: Int
    ): Int { // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) { // Calculate ratios of height and width to requested height and width
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
            val totalPixels = width * height.toFloat()
            val totalReqPixelsCap = reqWidth * reqHeight * 2.toFloat()
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun rotateImageIfRequired(
        context: Context,
        img: Bitmap,
        selectedImage: Uri
    ): Bitmap? {
        val input: InputStream? = context.getContentResolver().openInputStream(selectedImage)
        val ei: ExifInterface
        if (Build.VERSION.SDK_INT > 23) ei = ExifInterface(input) else ei =
            ExifInterface(selectedImage.path)
        val orientation =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(img, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(img, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(img, 270)
            else -> img
        }
    }

    private fun rotateImage(source: Bitmap, angle: Int): Bitmap? {
        val matrix: Matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        val rotatedImg: Bitmap = Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
        source.recycle()
        return rotatedImg
    }

    private fun saveUserImage() {

        if (img.value != "@drawable/itemicont") {

            val file: Uri = img.value!!.toUri()

            //Toast.makeText(context as AppCompatActivity, file.toString(), Toast.LENGTH_LONG).show()
            val usersRef = storageRef.child("user_images/" + getUser() + "/"+itemId+ ".jpg")
            val uploadTask = usersRef.putFile(file)

            // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener { Log.d("Storage","error occurred during image upload") }
                .addOnSuccessListener { Log.d("Storage", "file caricato") }

        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        Log.d("dialog","positive")
        dialog as MapDialogFragment

        if(dialog.loc!= null && dialog.loc!= LatLng(0.0, 0.0)){
            Log.d("location", dialog.loc!!.toString())
            coords=dialog.loc
            coordsNotSet=false
            val geocoder : Geocoder= Geocoder((context as AppCompatActivity), Locale.getDefault())
            val addresses : List<Address> = geocoder.getFromLocation(coords.latitude, coords.longitude, 1)
            if(addresses.isNotEmpty())
                location.text.apply {
                    if(addresses[0].thoroughfare!=null || addresses[0].locality!=null) {
                        clear()
                        append(
                            (if (addresses[0].thoroughfare != null) {
                                addresses[0].thoroughfare.toString() + (if (addresses[0].featureName != null) " " + addresses[0].featureName.toString() else "") + ", "
                            } else "")
                                    + if (addresses[0].locality != null) addresses[0].locality.toString()
                            else ""
                        )
                    }
                }
        }
    }

    private fun showDialog(){
        val maps : MapDialogFragment = MapDialogFragment.newInstance(coords, Action.EDIT)
        maps.setTargetFragment(this, 1)
        maps.show(parentFragmentManager, "MapDialogFragment")
    }
}