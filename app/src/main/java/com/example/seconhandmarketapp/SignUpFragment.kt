package com.example.seconhandmarketapp

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.location.Address
import android.location.Geocoder
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.example.seconhandmarketapp.Users.immaginetemp
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.log_in.*
import kotlinx.android.synthetic.main.sign_up.*
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.util.*
import kotlinx.android.synthetic.main.fragment_edit_profile.full_name as full_name1
import kotlinx.android.synthetic.main.fragment_edit_profile.location as location1
import kotlinx.android.synthetic.main.fragment_edit_profile.nickname as nickname1
import kotlinx.android.synthetic.main.fragment_edit_profile.phone as phone1

class SignUpFragment : Fragment(), MapDialogFragment.MapDialogListener {

    lateinit var password: EditText
    lateinit var email: EditText
    private var img = MutableLiveData<String>()
    private var currentPhotoUri = MutableLiveData<Uri>()
    private var fromCamera = MutableLiveData<Boolean>()
    private var fromGallery = MutableLiveData<Boolean>()
    private val PICK_IMAGE = 202
    private val vm: MyViewModel by activityViewModels()

    private var coords : LatLng = LatLng(0.0,0.0)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR;

        return inflater.inflate(R.layout.sign_up, container, false)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingPanelSignUp.setVisibility(View.GONE);
        email=email_add
        password=passwd
        loc_button_sign_up.setOnClickListener {
            showDialog()
        }

        val cameraButton = view.findViewById<ImageButton>(R.id.cameraButton)
        cameraButton.setOnClickListener {
            it.showContextMenu()
        }
        registerForContextMenu(cameraButton)
        registration.setOnClickListener {
            if(validateEmail() && validatePassword()) {
                Users.loggedUser?.img = img.value!!
                Users.loggedUser?.name = full_name.text.toString()
                Users.loggedUser?.nick = nickname.text.toString()
                Users.loggedUser?.mail = email_add.text.toString()
                Users.loggedUser?.tel = phone.text.toString()
                Users.loggedUser?.loc = location.text.toString()
                Users.loggedUser?.coords = coords
                if(img.value!=null)  {
                    Users.immaginetemp = img.value!!
                }
                else immaginetemp=""
                if (activity is Auth) {
                    val myactivity: Auth? = activity as Auth?
                    myactivity!!.createAccount(email.text.toString(),password.text.toString())
                }}
            }
        back_to_log_in.setOnClickListener{
            findNavController().navigate(R.id.action_signUpFragment_to_logInFragment)
        }


    }

    private fun validateEmail(): Boolean{
        if(email.text.isEmpty())
        {
            email.setError(getString(R.string.mailempty))
            return false
        }
        else
        {
            if(email.text.contains("@") && email.text.contains("."))
            {
                return true
            }
            else
            {
                email.setError(getString(R.string.mailerror))
                return false
            }
        }
    }

    private fun validatePassword(): Boolean{
        if(password.text.isEmpty())
        {
            password.setError(getString(R.string.pwsempty))
            return false
        }
        else
        {
            if(password.text.length>5)
            {
                return true
            }
            else
            {
                password.setError(getString(R.string.pwserror))
                return false
            }
        }
    }



     override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        (context as AppCompatActivity).menuInflater.inflate(R.menu.context_menu, menu)
    }

 override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.photo -> {
                dispatchTakePictureIntent()
                true
            }
            R.id.image_gallery -> {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_OPEN_DOCUMENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }




    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = ""//SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        )
    }

    private val REQUEST_TAKE_PHOTO = 3

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity((context as AppCompatActivity).packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        (context as AppCompatActivity),
                        "com.example.seconhandmarketapp.fileprovider",
                        it
                    )
                    currentPhotoUri.value = photoURI;
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    private fun dispatchCropImageIntent(imageUri: Uri?) {
        val intent = CropImage.activity(imageUri)
            .setMinCropWindowSize(300,400)
            .setAspectRatio(3,4)
            .getIntent(requireContext());

        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val imageView = (context as AppCompatActivity).findViewById<ImageView>(R.id.image)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == AppCompatActivity.RESULT_OK) {
            dispatchCropImageIntent(currentPhotoUri.value)

            fromCamera.value = true
            fromGallery.value = false
        }
        if (requestCode == PICK_IMAGE && data != null) {
            // success
            dispatchCropImageIntent(data.data!!)

            fromCamera.value = false
            fromGallery.value = true
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == AppCompatActivity.RESULT_OK) {
                val rotatedBitmap = handleSamplingAndRotationBitmap(
                    (context as AppCompatActivity),
                    result.getUri()
                )
                imageView?.setImageURI(result.getUri())
                img.value = result.getUri().toString()
            }
        }
    }

    @Throws(IOException::class)
    fun handleSamplingAndRotationBitmap(
        context: Context,
        selectedImage: Uri?
    ): Bitmap? {
        val MAX_HEIGHT = 1024
        val MAX_WIDTH = 1024
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var imageStream: InputStream? = selectedImage?.let {
            context.getContentResolver().openInputStream(
                it
            )
        }
        BitmapFactory.decodeStream(imageStream, null, options)
        imageStream?.close()
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        imageStream = selectedImage?.let { context.getContentResolver().openInputStream(it) }!!
        var img = BitmapFactory.decodeStream(imageStream, null, options)
        img = selectedImage?.let { img?.let { it1 -> rotateImageIfRequired(context, it1, it) } }
        return img
    }

    private fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int, reqHeight: Int
    ): Int { // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) { // Calculate ratios of height and width to requested height and width
            val heightRatio =
                Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio =
                Math.round(width.toFloat() / reqWidth.toFloat())
            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
// with both dimensions larger than or equal to the requested height and width.
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
            // This offers some additional logic in case the image has a strange
// aspect ratio. For example, a panorama may have a much larger
// width than height. In these cases the total pixels might still
// end up being too large to fit comfortably in memory, so we should
// be more aggressive with sample down the image (=larger inSampleSize).
            val totalPixels = width * height.toFloat()
            // Anything more than 2x the requested pixels we'll sample down further
            val totalReqPixelsCap = reqWidth * reqHeight * 2.toFloat()
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun rotateImageIfRequired(
        context: Context,
        img: Bitmap,
        selectedImage: Uri
    ): Bitmap? {
        val input: InputStream? = context.getContentResolver().openInputStream(selectedImage)
        val ei: ExifInterface
        if (Build.VERSION.SDK_INT > 23) ei = ExifInterface(input) else ei =
            ExifInterface(selectedImage.path)
        val orientation =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(img, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(img, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(img, 270)
            else -> img
        }
    }

    fun rotateImage(source: Bitmap, angle: Int): Bitmap? {
        val matrix: Matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        val rotatedImg: Bitmap = Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
        source.recycle()
        return rotatedImg
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        Log.d("dialgo","positive")
        dialog as MapDialogFragment

        if(dialog.loc!= null && dialog.loc!= LatLng(0.0, 0.0)){
            Log.d("location", dialog.loc!!.toString())
            coords=dialog.loc
            val geocoder : Geocoder= Geocoder((context as AppCompatActivity), Locale.getDefault())
            val addresses : List<Address> = geocoder.getFromLocation(coords.latitude, coords.longitude, 1)
            if(addresses.isNotEmpty())
                location.text.apply {
                    if(addresses[0].thoroughfare!=null || addresses[0].locality!=null) {
                        clear()
                        append(
                            (if (addresses[0].thoroughfare != null) {
                                addresses[0].thoroughfare.toString() + (if (addresses[0].featureName != null) " " + addresses[0].featureName.toString() else "") + ", "
                            } else "")
                                    + if (addresses[0].locality != null) addresses[0].locality.toString()
                            else ""
                        )
                    }
                }
        }
    }

    private fun showDialog(){
        val maps : MapDialogFragment = MapDialogFragment.newInstance(if(coords==LatLng(0.0,0.0)) {vm.updateCurrentLoc(); vm.currentLoc} else coords, Action.EDIT)
        maps.setTargetFragment(this, 1)
        maps.show(parentFragmentManager, "MapDialogFragment")
    }
}