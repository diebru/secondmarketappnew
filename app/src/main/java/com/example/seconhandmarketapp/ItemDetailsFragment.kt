package com.example.seconhandmarketapp

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.item_show_fragment.*
import kotlinx.android.synthetic.main.item_show_fragment.image
import kotlinx.android.synthetic.main.item_show_fragment.location
import kotlinx.android.synthetic.main.item_show_fragment.ratingBar
import java.io.IOException
import java.io.InputStream

class ItemDetailsFragment : Fragment(), ReviewDialogFragment.NoticeDialogListener
{
    private var img = MutableLiveData<String>()
    private var titolo = MutableLiveData<String>()
    private var desc = MutableLiveData<String>()
    private var prezzo = MutableLiveData<String>()
    private var categoria = MutableLiveData<String>()
    private var loc = MutableLiveData<String>()
    private var exp = MutableLiveData<String>()
    private var int = MutableLiveData<String>()
    lateinit var coords : LatLng
    private var i=1
    private var fromCamera = MutableLiveData<Boolean>()
    private var fromGallery = MutableLiveData<Boolean>()
    private var default_pic = true
    private var saveState = false
    private var seller = true
    private var bool_sold = false
    private var bool_hide = false
    private var userID = ""
    private var itemID = ""
    private var userInt = arrayListOf("")
    private var buyer = ""
    private val db = Firebase.firestore
    private lateinit var listener: ListenerRegistration
    private val vm: MyViewModel by activityViewModels()
    private var fromBoughtItemList = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.setother(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.item_show_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("item", "${arguments?.getString("itemID")}")
        if(arguments?.getString("itemID") != null)
            itemID = arguments?.getString("itemID")!!
        if(arguments?.getString("userID") != null)
            userID = arguments?.getString("userID")!!
        if (arguments?.getBoolean("buyer")  != null)
            fromBoughtItemList = arguments?.getBoolean("buyer", false)!!
        initialize()

        if (arguments?.getString("img") != null) {
            img.value = arguments?.getString("img")
            if (img.value != "@drawable/itemicon")    image.setImageURI(img.value?.toUri())
        }

        if (savedInstanceState != null) {
            img.value = savedInstanceState.getString("img")
            fromCamera.value = (savedInstanceState.getBoolean("fromCamera"))
            fromGallery.value = (savedInstanceState.getBoolean("fromGallery"))
        }

        sellerButton()
        listButton()
    }

    private fun initialize() {
        titolo.observe(context as AppCompatActivity, Observer { title.text = it })
        desc.observe(context as AppCompatActivity, Observer { description.text = it })
        prezzo.observe(context as AppCompatActivity, Observer { price.text = it })
        categoria.observe(context as AppCompatActivity, Observer { category.text = it })
        loc.observe(context as AppCompatActivity, Observer { location.text = it })
        exp.observe(context as AppCompatActivity, Observer { expiry_date.text = it })
        int.observe(context as AppCompatActivity, Observer { nInterested.text = it })
        img.value = "@drawable/itemicon"
        titolo.value = ""
        desc.value = ""
        prezzo.value = ""
        categoria.value = ""
        loc.value = ""
        exp.value = ""
        int.value = "0 people interested"
        vm.updateCurrentLoc()
        coords = vm.currentLoc
        fromCamera.value = false
        fromGallery.value = false
        saveState = true

        loc_button.setOnClickListener {
            showDialog()
        }

        seller = userID == Users.getUser()!!
        if(!seller) {
            bInvisible.visibility = View.GONE
            bVisible.visibility = View.GONE
        }
        else
            nInterested.visibility = View.GONE
        listener = db.collection("users").document(userID).collection("items").document(itemID).addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w("ListenerLog", "Listen failed.", e)
                return@addSnapshotListener
            }
            if (snapshot != null && snapshot.exists()) {
                Log.d("ListenerLog", "Current data: ${snapshot.data}")
                val d = snapshot.data
                itemID = d?.get("itemID").toString()
                titolo.value = d?.get("title").toString()
                userID = d?.get("userID").toString()
                desc.value = d?.get("description").toString()
                exp.value = d?.get("expirydate").toString()
                prezzo.value = d?.get("price").toString()
                loc.value = d?.get("location").toString()
                categoria.value = d?.get("category").toString()
                bool_hide = d?.get("isHide") as Boolean
                bool_sold = d?.get("isSold") as Boolean
                userInt = d["interestedUsers"] as ArrayList<String>
                buyer = d["buyer"].toString()
                coords = if(d["coords"]!=null) {
                    val l = d["coords"].toString().split(",")
                    LatLng(l[0].toDouble(), l[1].toDouble())
                }
                else vm.currentLoc
                int.value = (if(userInt[0]=="") "0 people are" else if (userInt.size==1) {
                    if(userInt.contains(Users.getUser()))
                        "Only you are"
                    else
                        "1 person is"
                } else if(userInt.contains(Users.getUser())) {
                    "You and "+(userInt.size-1)+" other "+
                    if(userInt.size==2)
                        "person are"
                    else
                        "people are"}
                else userInt.size.toString()+" people are") + " interested"

                if(view != null) {
                    eye()
                    if (buyer == Users.getUser()) {
                        fab_item.visibility = View.GONE
                        nInterested.visibility = View.GONE
                    } else fabButton()
                    if (buyer != "") {
                        if (d.containsKey("comment")) {
                            comment.text = d["comment"].toString()
                            comment.visibility = View.VISIBLE
                        }
                        if (d.containsKey("rating")) {
                            ratingBar.rating = (d["rating"] as Double).toFloat()
                            ratingBar.visibility = View.VISIBLE
                            nick_buyer.text = d["nick_buyer"].toString()
                            nick_buyer.visibility = View.VISIBLE
                            review.visibility = View.VISIBLE
                        }
                    }
                }
            } else {
                Log.d("ListenerLog", "Current data: null")
            }
        }
    }

    private fun eye() {
        if(!bool_sold)
            im_soldout.visibility = View.GONE
        if(bool_hide) {
            bVisible.visibility = View.GONE
            bInvisible.setOnClickListener { Toast.makeText(context as AppCompatActivity,"This item is hidden", Toast.LENGTH_LONG).show() }
        } else {
            bInvisible.visibility = View.GONE
            bVisible.setOnClickListener { Toast.makeText(context as AppCompatActivity,"This item is visible", Toast.LENGTH_LONG).show() }
        }
    }

    private fun listButton() {
        if(seller) {
            users_button.setOnClickListener {
                val lista: MutableList<CharSequence> = mutableListOf()
                for (member in userInt)
                    lista.add("")
                for ((i, member) in userInt.withIndex()) {
                    if (member != "") {
                        db.collection("users")
                            .document(member)
                            .get()
                            .addOnSuccessListener { res ->
                                val d = res.data
                                if (d != null)  lista[i] = d.getValue("nick_name").toString()
                                if (lista[lista.size - 1] != "" && lista.size > 0)
                                    AlertDialog.Builder(context as AppCompatActivity)
                                        .setTitle("Interesed Users: " + lista.size.toString())
                                        .setItems(lista.toTypedArray()) { dialog, which ->
                                            val selected = userInt[which]
                                            if (!(Users.viewedUser != null && Users.viewedUser!!.uid == selected)) {
                                                db.collection("users").document(selected).get()
                                                    .addOnSuccessListener { res ->
                                                        Users.viewedUser = User(
                                                            selected,
                                                            res.getString("full_name")!!,
                                                            res.getString("nick_name")!!,
                                                            res.getString("mail")!!,
                                                            res.getString("phone_number")!!,
                                                            res.getString("location")!!
                                                        )
                                                        val b = Bundle()
                                                        b.putString("userID", selected)
                                                        listener.remove()
                                                        findNavController().navigate(R.id.action_itemDetailsFragment_to_showProfileFragment, b)
                                                    }
                                            } else {
                                                val b = Bundle()
                                                b.putString("userID", selected)
                                                listener.remove()
                                                findNavController().navigate(R.id.action_itemDetailsFragment_to_showProfileFragment,b)
                                            }
                                        }
                                        .setNegativeButton("Back") { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                                        .create()
                                        .show()
                            }
                            .addOnFailureListener { Toast.makeText(context as AppCompatActivity,"Error to load users",Toast.LENGTH_LONG).show() }
                    } else {
                        AlertDialog.Builder(context as AppCompatActivity)
                            .setTitle("Interesed Users: 0")
                            .setNegativeButton("Back") { dialog: DialogInterface, which: Int -> dialog.cancel() }
                            .create()
                            .show()
                    }
                }
            }
        }
        else users_button.visibility = View.GONE
    }

    private fun sellerButton() {
        if(!seller) {
            profile_button.setOnClickListener {
                if (!(Users.viewedUser != null && Users.viewedUser!!.uid == userID)) {
                    db.collection("users").document(userID) //qui va messo l'effettivo uid
                        .get()
                        .addOnSuccessListener { res ->
                            Users.viewedUser = User(
                                userID,
                                res.getString("full_name")!!,
                                res.getString("nick_name")!!,
                                res.getString("mail")!!,
                                res.getString("phone_number")!!,
                                res.getString("location")!!,
                                coords =  if(res.getString("coords")!=null) {
                                    val l = res.getString("coords").toString().split(",")
                                    LatLng(l[0].toDouble(), l[1].toDouble())
                                }
                                else vm.currentLoc
                            )
                            val b = Bundle()
                            b.putString("userID", userID)
                            listener.remove()
                            findNavController().navigate(R.id.action_itemDetailsFragment_to_showProfileFragment, b)
                        }
                } else {
                    val b = Bundle()
                    b.putString("userID", userID)
                    listener.remove()
                    findNavController().navigate(R.id.action_itemDetailsFragment_to_showProfileFragment, b)
                }
            }
        }
        else profile_button.visibility = View.GONE
    }



    private fun fabButton() {
        var present = false
        val lista: MutableList<String> = mutableListOf()
        var empty = false

        if(!seller) {
            fab_item.show()
            for (member in userInt) {
                if(member == "")
                    empty = true
                else
                    lista.add(member)
                if (member == Users.getUser())
                    present = true
            }
            if(present)
                fab_item.backgroundTintList = ColorStateList.valueOf(0xFF4DB8FF.toInt())
            fab_item.setOnClickListener {
                listener.remove()
                ItemOfInterestList.setUpd(true)
                if(fab_item.backgroundTintList == ColorStateList.valueOf(0xFF4DB8FF.toInt())) {
                    fab_item.backgroundTintList = ColorStateList.valueOf(0xFFAAAAAA.toInt())
                    lista.remove(Users.getUser())
                } else {
                    fab_item.backgroundTintList = ColorStateList.valueOf(0xFF4DB8FF.toInt())
                    lista.add(Users.getUser()!!)
                    if (empty)  sendNotification(0, userID, itemID, Users.getUserData()?.nick.toString(), titolo.value.toString())
                    else        sendNotification(userInt.size, userID, itemID, Users.getUserData()?.nick.toString(), titolo.value.toString())
                }
                if(lista.size > 0)
                    db.collection("users").document(userID).collection("items").document(itemID).update("interestedUsers", lista.toList())
                        .addOnSuccessListener { Log.d("Update interested users", "Update list") }
                        .addOnFailureListener { Log.d("Update interested users", "Not Update list") }
                else
                    db.collection("users").document(userID).collection("items").document(itemID).update("interestedUsers", arrayListOf(""))
                        .addOnSuccessListener { Log.d("Update interested users", "Update list") }
                        .addOnFailureListener { Log.d("Update interested users", "Not Update list") }
            }
        }
        else fab_item.hide()
    }

    private fun sendNotification(docID: Int, recipientID: String, itemID: String, nickn: String, itemName: String) {
        val docData = hashMapOf(
            "recipientID" to recipientID,
            "itemID" to itemID,
            "nick_name" to nickn,
            "item_name" to itemName,
            "message" to "interessato"
        )

        db.collection("users").document(recipientID)
            .collection("items").document(itemID)
            .collection("notifications").document((docID+1).toString())
            .set(docData)
            .addOnSuccessListener { Log.d("Documento notifica", "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w("Documento notifica", "Error writing document", e) }
            .addOnCompleteListener { db.collection("users").document(recipientID)
                .collection("items").document(itemID)
                .collection("notifications").document((docID+1).toString()).delete() }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if(saveState) {
            outState.putString("img", img.value!!)
            outState.putBoolean("fromCamera", fromCamera.value!!)
            outState.putBoolean("fromGallery", fromGallery.value!!)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(seller) {
            inflater.inflate(R.menu.show_item_menu, menu)
            super.onCreateOptionsMenu(menu, inflater)
        }
        else if(fromBoughtItemList){
            inflater.inflate(R.menu.review_menu, menu)
            super.onCreateOptionsMenu(menu, inflater)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_edit -> {

            val b = Bundle()
            b.putString("itemID", itemID)
            b.putString("userID", userID)
            b.putString("img", img.value)
            b.putBoolean("cam", fromCamera.value!!)
            b.putBoolean("gall", fromGallery.value!!)
            b.putBoolean("default_pic", default_pic)
            b.putFloatArray("coords", floatArrayOf(coords.latitude.toFloat(), coords.longitude.toFloat()))
            b.putBoolean("fromDetails", true)

            saveState = false
            listener.remove()
            findNavController().navigate(R.id.action_itemDetailsFragment_to_itemEditFragment, b)

            true
        }
        R.id.review -> {
            if(ratingBar.rating != 0f){
                val alert : AlertDialog? = activity?.let {
                    val builder = AlertDialog.Builder(it)
                        builder.apply {
                            setMessage("Rating already included")
                            setPositiveButton("OK", DialogInterface.OnClickListener{ _, _ ->

                            })
                        }
                    builder.create()
                }
                alert?.show()
                true
            }
            else {
                val review: ReviewDialogFragment = ReviewDialogFragment()
                review.setTargetFragment(this, 0)
                review.show(parentFragmentManager, "ReviewDialogFragment")
                true
            }
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    @Throws(IOException::class)
    fun handleSamplingAndRotationBitmap(
        context: Context,
        selectedImage: Uri?
    ): Bitmap? {
        val maxHeight = 1024
        val maxWidth = 1024
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var imageStream: InputStream? = selectedImage?.let {
            context.getContentResolver().openInputStream(
                it
            )
        }
        BitmapFactory.decodeStream(imageStream, null, options)
        imageStream?.close()
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, maxWidth, maxHeight)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        imageStream = selectedImage?.let { context.getContentResolver().openInputStream(it) }!!
        var img = BitmapFactory.decodeStream(imageStream, null, options)
        img = selectedImage?.let { img?.let { it1 -> rotateImageIfRequired(context, it1, it) } }
        return img
    }

    private fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int, reqHeight: Int
    ): Int { // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) { // Calculate ratios of height and width to requested height and width
            val heightRatio =
                Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio =
                Math.round(width.toFloat() / reqWidth.toFloat())
            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
// with both dimensions larger than or equal to the requested height and width.
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
            // This offers some additional logic in case the image has a strange
// aspect ratio. For example, a panorama may have a much larger
// width than height. In these cases the total pixels might still
// end up being too large to fit comfortably in memory, so we should
// be more aggressive with sample down the image (=larger inSampleSize).
            val totalPixels = width * height.toFloat()
            // Anything more than 2x the requested pixels we'll sample down further
            val totalReqPixelsCap = reqWidth * reqHeight * 2.toFloat()
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun rotateImageIfRequired(
        context: Context,
        img: Bitmap,
        selectedImage: Uri
    ): Bitmap? {
        val input: InputStream? = context.getContentResolver().openInputStream(selectedImage)
        val ei: ExifInterface
        if (Build.VERSION.SDK_INT > 23) ei = ExifInterface(input) else ei =
            ExifInterface(selectedImage.path)
        val orientation =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(img, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(img, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(img, 270)
            else -> img
        }
    }

    private fun rotateImage(source: Bitmap, angle: Int): Bitmap? {
        val matrix: Matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        val rotatedImg: Bitmap = Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
        source.recycle()
        return rotatedImg
    }

    private fun showDialog(){
        val maps : MapDialogFragment = MapDialogFragment.newInstance(coords, if(seller) Action.SHOW else Action.SHOW_BUYABLE_ITEM)
        maps.setTargetFragment(this, 1)
        maps.show(parentFragmentManager, "MapDialogFragment")
    }
    override fun onDialogPositiveClick(dialog: DialogFragment) {
        dialog as ReviewDialogFragment
        comment.text = dialog.txt
        comment.visibility = View.VISIBLE
        ratingBar.rating = dialog.rating
        ratingBar.visibility = View.VISIBLE
        review.visibility = View.VISIBLE
        nick_buyer.text = Users.getUserData()!!.nick

        //salvo su firebase
        val docData = hashMapOf(
            "comment" to dialog.txt,
            "rating" to dialog.rating,
            "nick_buyer" to nick_buyer.text
        )

        db.collection("users").document(userID)
            .collection("items").document(itemID)
            .set(docData, SetOptions.merge())
            .addOnSuccessListener { Log.d("review", "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w("review", "Error writing document", e) }

    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {

    }

}